package application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import application.business.Aeropuerto;
import application.business.Asiento;
import application.business.Reserva;
import application.business.Usuario;
import application.business.Vuelo;
import application.exceptions.InvalidInitialTestingData;
import application.models.SearchModel;
import application.services.ConsultaService;
import application.services.ReservasService;
import application.services.UsuarioService;
import application.services.VueloService;

public abstract class AbstractTest extends AbstractProfilesSupport{
	
	@Autowired protected VueloService vueloService;
	@Autowired protected ReservasService reservaService;
	@Autowired protected UsuarioService usuarioService;
	@Autowired protected ConsultaService consultaService;
	
	protected final String USERNAME_JUAN="juan";
	protected final String USERNAME_LEANDRO="salo";
	
	
	protected Usuario getUserByUsername(String username) {
		return usuarioService.getUserByUsername(username);
	}
	
	private Vuelo getPrimerVuelo() {
		return vueloService.getVuelos().get(0);
	}
	
	protected Asiento getPrimerAsientoLibre() throws InvalidInitialTestingData {
		List<Asiento> asientosLibresPrimerVuelo= getPrimerVuelo().getAsientosLibres();
		
		if (asientosLibresPrimerVuelo.isEmpty())
			throw new InvalidInitialTestingData("El primer vuelo debe contener asientos no reservados");
		else
			return asientosLibresPrimerVuelo.get(0);
	}
	
	protected Asiento getPrimerAsientoLibre(Vuelo vuelo) {
		return vuelo.getAsientosLibres().get(0);
	}
	
	protected Usuario getUsuarioSalo() {
		return this.getUserByUsername(USERNAME_LEANDRO);
	}
	
	protected Usuario getUsuarioJuan() {
		return this.getUserByUsername(USERNAME_JUAN);
	}
	
	protected Asiento getPrimerAsientoReservadoPor(long idUsuario) throws InvalidInitialTestingData {
		return getPrimeraReservaDe(idUsuario).getAsientoReservado();
	}

	protected Reserva getPrimeraReservaDe(long idUsuario) throws InvalidInitialTestingData {
		List<Reserva> reservasActivas= reservaService.getReservasActivas(idUsuario);
		if (reservasActivas.isEmpty())
			throw new InvalidInitialTestingData("El usuario " + idUsuario + " debe tener alguna reserva");
		else
			return reservasActivas.get(0);
	}
	
	protected SearchModel getEmptySearchModel() {
		SearchModel searchModel= new SearchModel();
		searchModel.setVuelos(vueloService.getVuelos());
		searchModel.setAeropuertos(vueloService.getAeropuertos());
		return searchModel;
	}
	
	protected Aeropuerto getAeropuertoByName(String nombreAeropuerto) {
		return vueloService.getAeropuertos().stream()
			.filter(aeropuerto -> aeropuerto.getCiudad().equals(nombreAeropuerto))
			.findFirst().get();
	}
	
	protected void executeSearch(SearchModel search){
		vueloService.searchVuelosWithModel(search);
	}
	
}
