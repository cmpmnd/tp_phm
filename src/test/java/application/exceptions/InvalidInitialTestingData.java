package application.exceptions;

public class InvalidInitialTestingData extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidInitialTestingData() {
		super();
	}

	public InvalidInitialTestingData(String message) {
		super(message);
	}

	
}
