package application;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

public abstract class AbstractProfilesSupport {
	
	@Autowired Environment enviroment;
	
	protected List<String> getProfilesActives() {
		List<String> profiles= Arrays.asList(enviroment.getActiveProfiles());
		return profiles;
	}
	
	protected boolean isProfileActive(String profile) {
		return getProfilesActives().contains(profile);
	}
}
