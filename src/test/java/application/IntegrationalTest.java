package application;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import application.business.Asiento;
import application.business.Usuario;
import application.business.Vuelo;
import application.constants.CiudadConstants;
import application.constants.ErrorConstants;
import application.exceptions.BusinessException;
import application.exceptions.InvalidInitialTestingData;
import application.models.SearchModel;
import application.models.VueloModel;
import javassist.NotFoundException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class IntegrationalTest extends AbstractTest {

	@Rule
	public ExpectedException expectedEx = ExpectedException.none();
	
	@Test
	public void recuperarVuelos() {
		List<Vuelo> listaDeVuelosRecuperada=vueloService.getVuelos();
		assertThat(listaDeVuelosRecuperada.size(), greaterThan(0));
	}
	
	@Test
	@Transactional
	public void reservarAsiento() throws BusinessException, InvalidInitialTestingData {
		Usuario salo = getUsuarioSalo();
		int cantidadDeReservasDeSalo= reservaService.getReservasActivas(salo.getIdUsuario()).size();
		usuarioService.reservarAsiento(getPrimerAsientoLibre(), salo);
		
		assertThat(reservaService.getReservasActivas(salo.getIdUsuario()).size(), greaterThan(cantidadDeReservasDeSalo));
	}
	
	@Test
	@Transactional
	public void reservaDeAsientoConcurrente() throws BusinessException, InvalidInitialTestingData {
		expectedEx.expect(BusinessException.class);
		expectedEx.expectMessage(ErrorConstants.ASIENTO_YA_RESERVADO);
		
		Usuario salo = getUsuarioSalo();
		Usuario juan = getUsuarioJuan();
		Asiento asientoAReservar = getPrimerAsientoLibre();	

		usuarioService.reservarAsiento(asientoAReservar, salo);
		usuarioService.reservarAsiento(asientoAReservar, juan);
	}
	
	@Test
	@Transactional
	public void cancelarReserva() throws NotFoundException, InvalidInitialTestingData {
		Long idJuan= getUsuarioJuan().getIdUsuario();
		Long idReservaACancelar= getPrimeraReservaDe(idJuan).getIdReserva();
		
		int cantReservasJuan= reservaService.getReservasActivas(idJuan).size();
		reservaService.cancelarReserva(idReservaACancelar, idJuan);
		
		assertThat(reservaService.getReservasActivas(idJuan).size(), lessThan(cantReservasJuan));
	}

	@Test
	public void cancelarReservaAjena() throws NotFoundException, InvalidInitialTestingData {
		expectedEx.expect(NotFoundException.class);
	    expectedEx.expectMessage(ErrorConstants.RESERVA_NO_ENCONTRADA);
	    
	    Long idJuan= getUsuarioJuan().getIdUsuario();
	    Long idSalo= getUsuarioSalo().getIdUsuario();
	    reservaService.cancelarReserva(getPrimeraReservaDe(idJuan).getIdReserva(), idSalo);
	}
	
	@Test
	public void buscarVuelosPorOrigen() throws NotFoundException {
		SearchModel search= getEmptySearchModel();
		Long idAeropBsAs= getAeropuertoByName(CiudadConstants.BUENOS_AIRES).getIdAeropuerto();
		
		search.setOrigen(idAeropBsAs);
		executeSearch(search);
		
		List<VueloModel> resultados= search.getVuelosModel().stream().filter(vm -> vm.getOrigen().getIdAeropuerto().equals(idAeropBsAs)).collect(Collectors.toList());
		assertThat(resultados, is(not(empty())));
	}
	
	@Test
	public void buscarVuelosIgualOrigenQueDestino() throws NotFoundException {
		SearchModel search= getEmptySearchModel();
		
		Long idAeropBsAs= getAeropuertoByName(CiudadConstants.BUENOS_AIRES).getIdAeropuerto();
		
		search.setOrigen(idAeropBsAs);
		search.setDestino(idAeropBsAs);
		executeSearch(search);
		
		assertThat(search.getVuelosModel(), empty());
	}
	
	@Test
	public void buscarVueloUsandoTodosLosFiltros() throws NotFoundException {
		SearchModel search= getEmptySearchModel();
		
		Long idAeropBsAs= getAeropuertoByName(CiudadConstants.BUENOS_AIRES).getIdAeropuerto();
		Long idAeropCanada= getAeropuertoByName(CiudadConstants.CANADA).getIdAeropuerto();
		
		search.setOrigen(idAeropBsAs);
		search.setDestino(idAeropCanada);
		search.setFechaDesde(LocalDate.of(2010, 05, 25));
		search.setFechaHasta(LocalDate.of(2018, 10, 12));
		search.setMonto("7000");
		executeSearch(search);
		
		VueloModel primerYUnicoVueloEncontrado= search.getVuelosModel().get(0);
		
		assertThat(search.getVuelosModel().size(), equalTo(1));//Un solo vuelo desde BsAs hacia Canada
		assertThat(primerYUnicoVueloEncontrado.getCantidadAsientosFiltrados(), lessThan(primerYUnicoVueloEncontrado.getVuelo().getAsientosLibres().size()));
	}

	
	
	@Value("${spring.configuration.use_search_logs}") String useLogs;
	@Test
	public void logearBusquedaYVerificar(){
		Long idSalo= getUsuarioSalo().getIdUsuario();
		Object idConsulta=null;
		try {
			int sizeBefore = consultaService.getCantidadLogs();
			idConsulta= consultaService.saveSearchLog(getEmptySearchModel(), idSalo);
			if (!useLogs.equals("false"))
				assertThat(consultaService.getCantidadLogs(), greaterThan(sizeBefore));
		} finally {
			consultaService.deleteSearchLog(idConsulta);
		}
	}
	
}
