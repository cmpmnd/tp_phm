function seleccionarAsiento(asiento, monto) {
	$("#asientoElegido")[0].innerHTML=asiento;//Visual
	$("#posicionAsiento")[0].value=asiento;//Valor
	formatearMonto(monto);
	habilitarBotonReserva();
}

function habilitarBotonReserva() {
	$("#reservar").removeClass('disabled');
}

function formatearMonto(monto) {
	$("#monto")[0].innerHTML= accounting.formatMoney(monto,[symbol = "$"],[precision = 2],[thousand = "."],[decimal = ","],[format = "%s%v"]);
}

function reservarAsiento() {
	var posicionSeleccionada=$("#posicionAsiento")[0].value;
	var idVuelo=$("#idVuelo")[0].value;
	var link= "/reservarAsiento/" + idVuelo + "/" + posicionSeleccionada;
	if (posicionSeleccionada!="")
		openConfirmModal(link);
}