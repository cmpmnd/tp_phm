package application.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import application.business.Aeropuerto;
import application.business.Vuelo;
import application.constants.VuelosConstants;

public class SearchModel {
	private List<VueloModel> vuelosModel = new ArrayList<VueloModel>();
	
	private Set<Aeropuerto> aeropuertos = new HashSet<Aeropuerto>();

	private Long origen;
	private Long destino;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate fechaDesde;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate fechaHasta;

	@Pattern(regexp = VuelosConstants.NUMBERS_PATTERN, message = "'Monto Máximo' debe ser un valor numérico")
	@Length(max = 7, message = "'Monto Máximo' no debe superar los 7 dígitos")
	private String monto="";

	@AssertTrue(message="''Fecha desde'' no puede ser superior que ''Fecha hasta''")
	public boolean isDateRangeValid() {
		if (fechaDesde!=null && fechaHasta!=null)
			return !fechaDesde.isAfter(fechaHasta);
		return true;
	}
	
	private void vuelosToVuelosModel(List<Vuelo> vuelos) {
		vuelosModel= new ArrayList<>();
		vuelos.stream().forEach(vuelo -> vuelosModel.add(new VueloModel(vuelo)));
	}
	
	public void setVuelos(List<Vuelo> vuelos) {
		vuelosToVuelosModel(vuelos);
	}

	public boolean isNullSearched() {
		return (origen==null && destino==null && fechaDesde==null && fechaHasta==null && monto.isEmpty());
	}
	
	public boolean isVuelosEmpty(){
		return vuelosModel.isEmpty();
	}
	
	private Aeropuerto getAeropuertoById(Long idAeropuerto) {
		if (idAeropuerto==null) return null;
		return aeropuertos.stream()
				.filter(aeropuerto -> aeropuerto.getIdAeropuerto().equals(idAeropuerto))
				.findFirst()
				.get();
	}
	
	public Aeropuerto getAeropuertoOrigen() throws NoSuchElementException{
		try {
			return getAeropuertoById(origen);
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("No se encuentra el Aeropuerto Origen seleccionado");
		}
	}
	
	public Aeropuerto getAeropuertoDestino() throws NoSuchElementException{
		try {
			return getAeropuertoById(destino);
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("No se encuentra el Aeropuerto Destino seleccionado");
		}
	}
	
	//SETTERS & GETTERS
	public List<VueloModel> getVuelosModel() {
		return vuelosModel;
	}
	public void setVuelosModel(List<VueloModel> vuelosModel) {
		this.vuelosModel = vuelosModel;
	}
	public Long getOrigen() {
		return origen;
	}
	public void setOrigen(Long origen) {
		this.origen = origen;
	}
	public Long getDestino() {
		return destino;
	}
	public void setDestino(Long destino) {
		this.destino = destino;
	}
	public Set<Aeropuerto> getAeropuertos() {
		return aeropuertos;
	}
	public void setAeropuertos(Set<Aeropuerto> set) {
		this.aeropuertos = set;
	}
	public LocalDate getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(LocalDate fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public LocalDate getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(LocalDate fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	
	public String toString() {
		return "Vuelos: " + vuelosModel.size();
	}

}