package application.models;

import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.AssertTrue;

import org.springframework.format.annotation.DateTimeFormat;

import application.business.Consulta;

public class LogsModel {
	
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate fechaDesde;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate fechaHasta;
	
	List<Consulta> consultas;

	
	@AssertTrue(message="''Fecha desde'' no puede ser superior que ''Fecha hasta''")
	public boolean isDateRangeValid() {
		if (fechaDesde!=null && fechaHasta!=null)
			return !fechaDesde.isAfter(fechaHasta);
		return true;
	}
	
	public LocalDate getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(LocalDate fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public LocalDate getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(LocalDate fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public List<Consulta> getConsultas() {
		return consultas;
	}

	public void setConsultas(List<Consulta> consultas) {
		this.consultas = consultas;
	}
	
	
}
