package application.models;

import java.time.LocalDateTime;

import application.business.Aeropuerto;
import application.business.Vuelo;
import application.utils.DateUtils;

//Se crea un modelo de Vuelo para no modificar el negocio con parametros de filtros;
public class VueloModel {
	
	private Vuelo vuelo;
	private int cantidadAsientosFiltrados;

	public VueloModel(){}
	
	public VueloModel(Vuelo vuelo) {
		this.vuelo = vuelo;
		this.cantidadAsientosFiltrados = vuelo.getAsientosLibres().size();
	}
	
	public String getStringFechaDespegue() {
		return DateUtils.dateTimeToString(getFechaDespegue());
	}
	
	public String getStringFechaAterrizaje() {
		return DateUtils.dateTimeToString(getFechaAterrizaje());
	}
	
	public long getIdVuelo(){
		return vuelo.getIdVuelo();
	}
	
	public Aeropuerto getOrigen() {
		return vuelo.getOrigen();
	}
	public Aeropuerto getDestino() {
		return vuelo.getDestino();
	}
	public LocalDateTime getFechaDespegue() {
		return vuelo.getFechaDespegue();
	}
	public LocalDateTime getFechaAterrizaje() {
		return vuelo.getFechaAterrizaje();
	}
	public int getCantidadTramos() {
		return vuelo.getCantidadTramos();
	}
	
	public float getMontoByClaveAsiento(String claveAsiento) {
		return vuelo.getAsiento(claveAsiento).getCostoTotal();
	}
	
	public boolean isAsientosEmpty(){
		return cantidadAsientosFiltrados == 0;
	}
	
	//Setters & Getters

	public Vuelo getVuelo() {
		return vuelo;
	}

	public void setVuelo(Vuelo vuelo) {
		this.vuelo = vuelo;
	}
	public int getCantidadAsientosFiltrados() {
		return cantidadAsientosFiltrados;
	}
	public void setCantidadAsientosFiltrados(int asientosFiltrados) {
		this.cantidadAsientosFiltrados = asientosFiltrados;
	}
	
	
}
