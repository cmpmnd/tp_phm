package application.DAOs;

import application.business.Asiento;
import application.business.Usuario;
import application.exceptions.BusinessException;

public interface RepoUsuariosInterface {

	public Usuario getUserById (long idUsuario);
	
	public Usuario getLazyUserById (long idUsuario);
	
	public Usuario getUserByUsername (String username);
	
	public void agregarUsuario(Usuario usuario);
	
	public void reservarAsiento(Asiento asiento, Usuario loggedUser) throws BusinessException;
}
