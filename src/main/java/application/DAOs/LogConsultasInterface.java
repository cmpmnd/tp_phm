package application.DAOs;

import java.time.LocalDate;
import java.util.List;

import org.bson.types.ObjectId;

import application.business.Consulta;
import application.business.Usuario;
import application.models.SearchModel;

public interface LogConsultasInterface {

	public Object saveSearchLog(SearchModel model, Usuario loggedUser);

	public List<Consulta> getAllLogs(Usuario usuario);

	public List<Consulta> searchLogsBetweenDates(LocalDate fechaDesde, LocalDate fechaHasta, Usuario usuario);

	public Consulta getConsultaById(ObjectId idConsulta);

	public int getCantidadLogs();

	public void deleteSearchLog(Object idLog);

}