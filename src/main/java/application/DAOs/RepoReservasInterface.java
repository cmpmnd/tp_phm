package application.DAOs;

import java.util.List;

import application.business.Reserva;
import application.business.Usuario;
import javassist.NotFoundException;

public interface RepoReservasInterface {
	
	public List<Reserva> getReservasActivas(Usuario loggedUser);

	public void cancelarReserva(long idReserva, Usuario loggedUser) throws NotFoundException;
	
}
