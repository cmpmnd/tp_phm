package application.DAOs.implementationNeo4J.searchFilters;

import java.util.Map;

import application.models.SearchModel;
import application.utils.NeoSearchBuilder;

public class FiltroDestino extends AbstractFiltrosNeo4J {

	public FiltroDestino(SearchModel modelo) {
		super(modelo);
	}

	@Override
	protected boolean debeAgregarFiltro() {
		return modelo.getAeropuertoDestino()!=null;
	}

	@Override
	protected String queryConFiltro(String queryActual, Map<String, Object> params) {
		params.put("idAeropuertoDestino", modelo.getDestino());
		return NeoSearchBuilder.addFilterToQuery(queryActual, "id(destino)={idAeropuertoDestino}");
	}
}
