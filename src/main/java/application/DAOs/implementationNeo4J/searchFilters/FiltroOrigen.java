package application.DAOs.implementationNeo4J.searchFilters;

import java.util.Map;

import application.models.SearchModel;
import application.utils.NeoSearchBuilder;

public class FiltroOrigen extends AbstractFiltrosNeo4J {

	public FiltroOrigen(SearchModel modelo) {
		super(modelo);
	}

	@Override
	protected boolean debeAgregarFiltro() {
		return modelo.getAeropuertoOrigen()!=null;
	}

	@Override
	protected String queryConFiltro(String queryActual, Map<String, Object> params) {
		params.put("idAeropuertoOrigen", modelo.getOrigen());
		return NeoSearchBuilder.addFilterToQuery(queryActual, "id(origen)={idAeropuertoOrigen}");
	}
}
