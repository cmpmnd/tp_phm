package application.DAOs.implementationNeo4J.searchFilters;

import java.util.Map;

import application.models.SearchModel;

public abstract class AbstractFiltrosNeo4J {
	protected SearchModel modelo;
	
	public AbstractFiltrosNeo4J(SearchModel modelo) {
		this.modelo = modelo;
	}

	public String agregarFiltro(String queryActual, Map<String, Object> params) {
		if (debeAgregarFiltro())
			return queryConFiltro(queryActual, params);
		return queryActual;
	}

	protected abstract boolean debeAgregarFiltro();
	protected abstract String queryConFiltro(String queryActual, Map<String, Object> params);
}
