package application.DAOs.implementationNeo4J.searchFilters;

import java.time.LocalDateTime;
import java.util.Map;

import application.configurations.converters.neo4j.LocalDateTimeToLong;
import application.models.SearchModel;
import application.utils.DateUtils;
import application.utils.NeoSearchBuilder;

public class FiltroFechaDesde extends AbstractFiltrosNeo4J {

	public FiltroFechaDesde(SearchModel modelo) {
		super(modelo);
	}

	@Override
	protected boolean debeAgregarFiltro() {
		return modelo.getFechaDesde()!=null;
	}

	@Override
	protected String queryConFiltro(String queryActual, Map<String, Object> params) {
		LocalDateTime fechaDesdeEarly= DateUtils.localDateToLocalDateTimeEarly(modelo.getFechaDesde());
		params.put("fechaDesde", new LocalDateTimeToLong().convert(fechaDesdeEarly));
		return NeoSearchBuilder.addFilterToQuery(queryActual, "vuelo.fechaDespegue>={fechaDesde}");
	}
}
