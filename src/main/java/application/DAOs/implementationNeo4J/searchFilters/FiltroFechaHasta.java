package application.DAOs.implementationNeo4J.searchFilters;

import java.time.LocalDateTime;
import java.util.Map;

import application.configurations.converters.neo4j.LocalDateTimeToLong;
import application.models.SearchModel;
import application.utils.DateUtils;
import application.utils.NeoSearchBuilder;

public class FiltroFechaHasta extends AbstractFiltrosNeo4J {

	public FiltroFechaHasta(SearchModel modelo) {
		super(modelo);
	}

	@Override
	protected boolean debeAgregarFiltro() {
		return modelo.getFechaHasta()!=null;
	}

	@Override
	protected String queryConFiltro(String queryActual, Map<String, Object> params) {
		LocalDateTime fechaHastaLate= DateUtils.localDateToLocalDateTimeLate(modelo.getFechaHasta());
		params.put("fechaHasta", new LocalDateTimeToLong().convert(fechaHastaLate));
		return NeoSearchBuilder.addFilterToQuery(queryActual, "vuelo.fechaDespegue<={fechaHasta}");
	}
}
