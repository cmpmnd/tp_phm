package application.DAOs.implementationNeo4J;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import application.DAOs.RepoUsuariosInterface;
import application.business.Asiento;
import application.business.Usuario;
import application.constants.Profil;
import application.exceptions.BusinessException;

@Profile(Profil.NEO4J)
@Primary
@Repository
@Transactional
public class UsuarioNeo4J extends AbstractCommonNeo4J implements RepoUsuariosInterface {

	public void agregarUsuario(Usuario usuario) {
		neoTemplate.save(usuario);
	}

	public Usuario getUserById(long idUsuario) {
		Usuario user=neoTemplate.findOne(idUsuario, Usuario.class);
		neoTemplate.fetch(user.getReservas());
		return user;
	}

	public Usuario getLazyUserById(long idUsuario) {
		return neoTemplate.findOne(idUsuario, Usuario.class); //por defecto LAZY
	}

	public Usuario getUserByUsername(String username) {
		final String NEO_QUERY="MATCH (user:Usuario {username: {userName}}) RETURN user";
		
		Map<String, Object> params= new HashMap<>();
		params.put("userName", username);
		
		return neoTemplate.query(NEO_QUERY, params)
			.to(Usuario.class)
			.singleOrNull();
	}

	public void reservarAsiento(Asiento asiento, Usuario loggedUser) throws BusinessException {
		neoTemplate.save(loggedUser.reservar(asiento));
		neoTemplate.save(asiento);
	}

}