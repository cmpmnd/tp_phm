package application.DAOs.implementationNeo4J;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import application.DAOs.RepoReservasInterface;
import application.business.Reserva;
import application.business.Usuario;
import application.constants.ErrorConstants;
import application.constants.Profil;
import javassist.NotFoundException;

@Profile(Profil.NEO4J)
@Primary
@Repository
@Transactional
public class ReservaNeo4J extends AbstractCommonNeo4J implements RepoReservasInterface {

	@SuppressWarnings("unchecked")//El warning esta acarreado desde la clase compilada Neo4jTemplate;
	public List<Reserva> getReservasActivas(Usuario loggedUser) {
		final String NEO_QUERY= "MATCH (u:Usuario)-[r:RESERVO]->() WHERE ID(u)={idUsuario} AND r.fechaCancelacion IS NULL RETURN r";
		
		Map<String, Object> params= new HashMap<>();
		params.put("idUsuario", loggedUser.getIdUsuario());

		return neoTemplate
				.query(NEO_QUERY, params)
				.to(Reserva.class)
				.as(List.class);
	}
	
	public void cancelarReserva(long idReserva, Usuario loggedUser) throws NotFoundException {
		final String NEO_QUERY= "MATCH (user:Usuario)-[reserva:RESERVO]->() WHERE id(user)={idUsuario} AND id(reserva)={idReserva} RETURN reserva";
		Map<String, Object> params= new HashMap<>();
		params.put("idUsuario", loggedUser.getIdUsuario());
		params.put("idReserva", idReserva);
		
		Reserva reservaACancelar= neoTemplate.query(NEO_QUERY, params).to(Reserva.class).singleOrNull();
		
		if (reservaACancelar==null)//Solo se pueden cancelar reservas propias
			throw new NotFoundException(ErrorConstants.RESERVA_NO_ENCONTRADA);
		
		reservaACancelar.cancelarReserva();
		neoTemplate.save(reservaACancelar.getAsientoReservado());
		neoTemplate.saveOnly(reservaACancelar);
	}
}
