package application.DAOs.implementationNeo4J;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import application.DAOs.RepoVuelosInterface;
import application.DAOs.implementationNeo4J.searchFilters.FiltroDestino;
import application.DAOs.implementationNeo4J.searchFilters.FiltroFechaDesde;
import application.DAOs.implementationNeo4J.searchFilters.FiltroFechaHasta;
import application.DAOs.implementationNeo4J.searchFilters.FiltroOrigen;
import application.business.Aeropuerto;
import application.business.Asiento;
import application.business.Vuelo;
import application.constants.Profil;
import application.models.SearchModel;
import application.utils.NeoSearchBuilder;

@Profile(Profil.NEO4J)
@Primary
@Repository
@Transactional
public class VueloNeo4J extends AbstractCommonNeo4J implements RepoVuelosInterface {

	public Vuelo getVueloById(long vueloId) throws Exception {
		try {
			Vuelo vuelo = neoTemplate.findOne(vueloId, Vuelo.class);
			neoTemplate.fetch(vuelo.getAsientos());
			neoTemplate.fetch(vuelo.getTramos());
			return vuelo;
		} catch (Exception e) {
			throw new Exception("No se ha encontrado el vuelo solicitado");
		}
	}

	public void agregarVuelo(Vuelo vuelo) {
		neoTemplate.save(vuelo);
	}

	public void agregarAeropuerto(Aeropuerto aeropuerto) {
		neoTemplate.save(aeropuerto);
	}

	@SuppressWarnings("unchecked")//El warning esta acarreado desde la clase compilada Neo4jTemplate;
	public void updateSearchModel(SearchModel searchScreen) {
		String searchQuery = "MATCH (origen:Aeropuerto)<-[:DESDE]-(vuelo:Vuelo)-[:HASTA]->(destino:Aeropuerto)";
		Map<String, Object> params= new HashMap<>();
		
		//Filtros
		searchQuery= new FiltroOrigen(searchScreen).agregarFiltro(searchQuery, params);
		searchQuery= new FiltroDestino(searchScreen).agregarFiltro(searchQuery, params);
		searchQuery= new FiltroFechaDesde(searchScreen).agregarFiltro(searchQuery, params);
		searchQuery= new FiltroFechaHasta(searchScreen).agregarFiltro(searchQuery, params);

		searchQuery=NeoSearchBuilder.addReturnVariable(searchQuery, "vuelo"); //return
		
		List<Vuelo> vuelos= neoTemplate.query(searchQuery, params).to(Vuelo.class).as(List.class); //Ejecutar Búsqueda
		vuelos.forEach(vuelo -> neoTemplate.fetch(vuelo.getAsientos()));//Hidratación de asientos
		
		searchScreen.setVuelos(vuelos);
		
		//Filtro especial, se debe aplicar luego de ejecutar la búsqueda;
		if (!searchScreen.getMonto().isEmpty())
			searchScreen.getVuelosModel().forEach(vm -> vm.setCantidadAsientosFiltrados(getCantidadAsientosLibres(searchScreen.getMonto(), vm.getIdVuelo())));
	}

	@SuppressWarnings("unchecked")//El warning esta acarreado desde la clase compilada Neo4jTemplate;
	private int getCantidadAsientosLibres(String monto, long idVuelo) {
		final String NEO_QUERY = "MATCH (v:Vuelo)-[:ASIENTO]->(a:Asiento {estaReservado: false}) WHERE ID(v)={idVuelo} RETURN a";

		Map<String, Object> params = new HashMap<>();
		params.put("idVuelo", idVuelo);
		List<Asiento> asientos= neoTemplate.query(NEO_QUERY, params).to(Asiento.class).as(List.class);
		
		return asientos.stream()
				.filter(asiento -> asiento.getCostoTotal()< Float.parseFloat(monto))
				.collect(Collectors.toList()).size();
	}

	public List<Vuelo> getVuelos() {
		List<Vuelo> vuelos = resultAsList(neoTemplate.findAll(Vuelo.class));
		vuelos.forEach(vuelo -> neoTemplate.fetch(vuelo.getAsientos()));

		return vuelos;
	}

	public Set<Aeropuerto> getAeropuertos() {
		return resultAsSet(neoTemplate.findAll(Aeropuerto.class));
	}

	public boolean existenVuelos() {
		long cantidadDeVuelosPersistidos = neoTemplate.count(Vuelo.class);
		log.info("Se detectaron " + cantidadDeVuelosPersistidos + " vuelos con NEO4J");
		return (cantidadDeVuelosPersistidos > 0);
	}
}
