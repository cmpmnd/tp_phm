package application.DAOs.implementationNeo4J;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.conversion.Result;
import org.springframework.data.neo4j.support.Neo4jTemplate;

public abstract class AbstractCommonNeo4J {
	protected final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	protected Neo4jTemplate neoTemplate;
	
	protected final <T> List<T> resultAsList(Result<T> result) {
		List<T> ret= new ArrayList<T>();
		result.forEach(r -> ret.add(r));
		return ret;
	}
	
	protected final <T> Set<T> resultAsSet(Result<T> result) {
		Set<T> ret= new HashSet<T>();
		result.forEach(r -> ret.add(r));
		return ret;
	}
}
