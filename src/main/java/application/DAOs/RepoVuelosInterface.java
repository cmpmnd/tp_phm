package application.DAOs;

import java.util.List;
import java.util.Set;

import application.business.Aeropuerto;
import application.business.Vuelo;
import application.models.SearchModel;
import javassist.NotFoundException;

public interface RepoVuelosInterface {
	
	public Vuelo getVueloById(long vueloId) throws NotFoundException, Exception;
	
	public void agregarVuelo(Vuelo vuelo);
	
	public void agregarAeropuerto(Aeropuerto aeropuerto);
	
	public void updateSearchModel(SearchModel searchScreen);
	
	public List<Vuelo> getVuelos();
	
	public Set<Aeropuerto> getAeropuertos();
	
	public boolean existenVuelos();
	
}
