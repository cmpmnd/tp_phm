package application.DAOs;

public interface IdProviderInterface {
	
	public Long getIdForVuelo();

	public Long getIdForReserva();
	
	public Long getIdForUsuario();
	
	public Long getIdForAeropuerto();
	
	public Long getIdForAsiento();
	
	public Long getIdForTarifa();
	
	public Long getIdForTramo();
	
	public void resetAllIds();
	
}
