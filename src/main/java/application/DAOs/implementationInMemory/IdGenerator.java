package application.DAOs.implementationInMemory;

import org.springframework.stereotype.Repository;

import application.DAOs.IdProviderInterface;

@Repository
public class IdGenerator implements IdProviderInterface {

	private Long idVuelo=new Long(1);
	private Long idReserva=new Long(1);
	private Long idUsuario=new Long(1);
	private Long idAeropuerto=new Long(1);
	private Long idAsiento=new Long(1);
	private Long idTarifa=new Long(1);
	private Long idTramo=new Long(1);
	
	
	public Long getIdForVuelo() {
		return idVuelo++;
	}

	public Long getIdForReserva() {
		return idReserva++;
	}

	public Long getIdForUsuario() {
		return idUsuario++;
	}

	public Long getIdForAeropuerto() {
		return idAeropuerto++;
	}

	public Long getIdForAsiento() {
		return idAsiento++;
	}
	
	public Long getIdForTarifa() {
		return idTarifa++;
	}

	public Long getIdForTramo() {
		return idTramo++;
	}

	public void resetAllIds() {
		this.idVuelo=new Long(1);
		this.idReserva=new Long(1);
		this.idUsuario=new Long(1);
		this.idAeropuerto=new Long(1);
		this.idAsiento=new Long(1);
		this.idTarifa=new Long(1);
		this.idTramo=new Long(1);
	}

}
