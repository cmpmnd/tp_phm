package application.DAOs.implementationInMemory.searchFilters;

import java.util.List;

import application.models.VueloModel;

public interface SearcherDecorator {
	
	public List<VueloModel> search(); 

}
