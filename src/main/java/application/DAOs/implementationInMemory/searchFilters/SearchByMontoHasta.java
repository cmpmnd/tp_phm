package application.DAOs.implementationInMemory.searchFilters;

import java.util.List;
import java.util.stream.Collectors;

import application.business.Vuelo;
import application.models.VueloModel;

public class SearchByMontoHasta extends SearcherAbstract {

	private String montoHasta;

	public SearchByMontoHasta(String montoHasta, SearcherDecorator consulta) {
		super(consulta);
		descripcion = "Por monto hasta: " + montoHasta;
		this.montoHasta = montoHasta;
	}

	protected boolean shallFilter() {
		return !montoHasta.isEmpty();
	}

	protected List<VueloModel> doFilter() {
		List<VueloModel> results= previousResults();
		results.forEach(vueloM -> vueloM.setCantidadAsientosFiltrados(
				getCantidadAsientosByMontoHasta(
						vueloM.getVuelo(), getValueOfMontoHasta())));
		return results;
	}
	
	private int getCantidadAsientosByMontoHasta(Vuelo vuelo, float monto){
		return vuelo.getAsientosLibres().stream()
				.filter(asiento -> asiento.getCostoTotal() <= monto)
				.collect(Collectors.toList()).size();
	}
	
	private float getValueOfMontoHasta() {
		return Float.parseFloat(montoHasta);
	}
}
