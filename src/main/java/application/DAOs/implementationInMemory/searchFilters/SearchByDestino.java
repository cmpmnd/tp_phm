package application.DAOs.implementationInMemory.searchFilters;

import java.util.List;
import java.util.stream.Collectors;

import application.models.VueloModel;

public class SearchByDestino extends SearcherAbstract {

	private Long idAeropuertoDestino;

	public SearchByDestino(Long idAeropuerto, SearcherDecorator consulta){
		super(consulta);
		descripcion= "Por Aeropuerto de Destino: " + idAeropuerto;
		this.idAeropuertoDestino= idAeropuerto;
	}

	protected boolean shallFilter() {
		return idAeropuertoDestino!=null;
	}

	protected List<VueloModel> doFilter() {
		return searcher.search().stream()
				.filter(vuelo -> vuelo.getDestino().getIdAeropuerto().equals(idAeropuertoDestino))
				.collect(Collectors.toList());
	}
}
