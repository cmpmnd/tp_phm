package application.DAOs.implementationInMemory.searchFilters;

import java.time.LocalDate;

import application.models.SearchModel;

public class SearcherIntermediator {
	SearchModel searchModel;

	public SearcherIntermediator(SearchModel searchModel) {
		this.searchModel = searchModel;
	}

	public void search(){
		searchModel.setVuelosModel(
			new SearchByMontoHasta(getMontoHasta(),
				new SearchByFechaHasta(getFechaHasta(), 
					new SearchByFechaDesde(getFechaDesde(), 
						new SearchByDestino(getDestino(), 
							new SearchByOrigen(getOrigen(), 
								getSearcherForDecorator()))))).search());
	}

	
	private Searcher getSearcherForDecorator() {
		return new Searcher(searchModel.getVuelosModel());
	}

	private Long getOrigen() {
		return searchModel.getOrigen();
	}
	private Long getDestino() {
		return searchModel.getDestino();
	}
	private LocalDate getFechaDesde() {
		return searchModel.getFechaDesde();
	}
	private LocalDate getFechaHasta() {
		return searchModel.getFechaHasta();
	}
	private String getMontoHasta() {
		return searchModel.getMonto();
	}
}
