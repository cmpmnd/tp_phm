package application.DAOs.implementationInMemory.searchFilters;

import java.util.List;
import java.util.stream.Collectors;

import application.models.VueloModel;

public class SearchByOrigen extends SearcherAbstract {

	private Long idAeropuertoOrigen;

	public SearchByOrigen(Long idAeropuerto, SearcherDecorator consulta){
		super(consulta);
		descripcion= "Por Aeropuerto de Origen: " + idAeropuerto;
		this.idAeropuertoOrigen= idAeropuerto;
	}

	protected boolean shallFilter() {
		return idAeropuertoOrigen!=null;
	}

	protected List<VueloModel> doFilter() {
		return searcher.search().stream()
				.filter(vuelo -> vuelo.getOrigen().getIdAeropuerto().equals(idAeropuertoOrigen))
				.collect(Collectors.toList());
	}
}