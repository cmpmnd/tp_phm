package application.DAOs.implementationInMemory.searchFilters;

import java.util.List;

import application.models.VueloModel;

public abstract class SearcherAbstract implements SearcherDecorator {

		protected SearcherDecorator searcher;
		protected String descripcion;

		protected SearcherAbstract(SearcherDecorator searcher) {
			this.searcher = searcher;
		}
		
		public final List<VueloModel> search() {
			if (shallFilter())
				return doFilter();
			else
				return previousResults();
		}
		
		protected List<VueloModel> previousResults() {
			return searcher.search();
		}
		
		protected abstract boolean shallFilter();
		protected abstract List<VueloModel> doFilter();
}
