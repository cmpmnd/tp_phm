package application.DAOs.implementationInMemory.searchFilters;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import application.models.VueloModel;
import application.utils.DateUtils;

public class SearchByFechaDesde extends SearcherAbstract {

	private LocalDate fechaDesde;

	public SearchByFechaDesde(LocalDate fechaDesde, SearcherDecorator consulta){
		super(consulta);
		descripcion= "Por fecha desde: " + fechaDesde;
		this.fechaDesde= fechaDesde;
	}
	
	protected boolean shallFilter() {
		return fechaDesde!=null;
	}
	
	protected List<VueloModel> doFilter() {
		LocalDateTime fechaDesdeEarly= DateUtils.localDateToLocalDateTimeEarly(fechaDesde);
		return previousResults().stream()
				.filter(vuelo -> vuelo.getFechaDespegue().isAfter(fechaDesdeEarly))
				.collect(Collectors.toList());
	}
}
