package application.DAOs.implementationInMemory.searchFilters;

import java.util.ArrayList;
import java.util.List;

import application.models.VueloModel;

public class Searcher implements SearcherDecorator {

	private List<VueloModel> todosLosVuelos= new ArrayList<VueloModel>();
	
	public Searcher(List<VueloModel> todosLosVuelos) {
		this.todosLosVuelos = todosLosVuelos;
	}

	@Override
	public List<VueloModel> search() {
		return todosLosVuelos;
	}

}
