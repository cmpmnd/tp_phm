package application.DAOs.implementationInMemory.searchFilters;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import application.models.VueloModel;
import application.utils.DateUtils;

public class SearchByFechaHasta extends SearcherAbstract {

	private LocalDate fechaHasta;

	public SearchByFechaHasta(LocalDate fechaHasta, SearcherDecorator consulta){
		super(consulta);
		descripcion= "Por fecha hasta: " + fechaHasta;
		this.fechaHasta= fechaHasta;
	}
	
	protected boolean shallFilter() {
		return fechaHasta!=null;
	}
	
	protected List<VueloModel> doFilter() {
		LocalDateTime fechaHastaLate= DateUtils.localDateToLocalDateTimeLate(fechaHasta);
		return previousResults().stream()
				.filter(vuelo -> vuelo.getFechaDespegue().isBefore(fechaHastaLate))
				.collect(Collectors.toList());
	}
}
