package application.DAOs.implementationInMemory;

import java.util.List;

import org.springframework.stereotype.Repository;

import application.DAOs.RepoReservasInterface;
import application.business.Reserva;
import application.business.Usuario;
import javassist.NotFoundException;

@Repository
public class RepoReservas implements RepoReservasInterface {
	
	public List<Reserva> getReservasActivas(Usuario loggedUser) {
		return loggedUser.getReservasActivas();
	}
	
	public void cancelarReserva(long idReserva, Usuario loggedUser) throws NotFoundException {
		loggedUser.cancelarReserva(idReserva);
	}

}
