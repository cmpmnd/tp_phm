package application.DAOs.implementationInMemory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import org.springframework.stereotype.Repository;

import application.DAOs.RepoVuelosInterface;
import application.DAOs.implementationInMemory.searchFilters.SearcherIntermediator;
import application.business.Aeropuerto;
import application.business.Vuelo;
import application.models.SearchModel;
import javassist.NotFoundException;

@Repository
public class RepoVuelos implements RepoVuelosInterface {
	List<Vuelo> vuelos= new ArrayList<Vuelo>();
	Set<Aeropuerto> aeropuertos = new HashSet<Aeropuerto>();
	
	public Vuelo getVueloById(long vueloId) throws NotFoundException {
		try {
			return vuelos.stream()
					.filter(r -> r.getIdVuelo() == vueloId)
					.findFirst()
					.get();
		} catch (NoSuchElementException e) {
			throw new NotFoundException("No se ha encontrado el vuelo solicitado");
		}
	}
	
	public void agregarVuelo(Vuelo vuelo) {
		aeropuertos.add(vuelo.getOrigen());
		aeropuertos.add(vuelo.getDestino());
		
		vuelos.add(vuelo);
	}
	
	public void updateSearchModel(SearchModel searchScreen){
		searchScreen.setVuelos(vuelos);
		new SearcherIntermediator(searchScreen).search();
	}
	
	public List<Vuelo> getVuelos() {
		return vuelos;
	}
	
	public Set<Aeropuerto> getAeropuertos() {
		return aeropuertos;
	}
	
	public void resetRepository() {
		vuelos= new ArrayList<Vuelo>();
		aeropuertos=new HashSet<Aeropuerto>();
	}

	
	public void agregarAeropuerto(Aeropuerto aeropuerto) {
		aeropuertos.add(aeropuerto);		
	}

	public boolean existenVuelos() {
		return !vuelos.isEmpty();
	}
	
}
