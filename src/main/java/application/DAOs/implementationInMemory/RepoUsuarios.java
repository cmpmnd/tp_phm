package application.DAOs.implementationInMemory;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import application.DAOs.IdProviderInterface;
import application.DAOs.RepoUsuariosInterface;
import application.business.Asiento;
import application.business.Reserva;
import application.business.Usuario;
import application.exceptions.BusinessException;

@Repository
public class RepoUsuarios implements RepoUsuariosInterface {
	private List<Usuario> usuarios = new ArrayList<Usuario>();
	@Autowired private IdProviderInterface idProvider;

	//Used by Spring Security
	public Usuario getUserByUsername(String username) {
		return usuarios.stream().filter(u -> u.getUsername().equals(username)).findFirst().get();
	}
	
	public Usuario getUserById (long idUsuario) {
		return usuarios.stream().filter(u -> u.getIdUsuario() == idUsuario).findFirst().get();
	}

	//Trae al usuario sin las reservas;
	public Usuario getLazyUserById(long idUsuario) {
		return getUserById(idUsuario);//No aplica cuando es inMemory
	}
	
	public void agregarUsuario(Usuario usuario) {
		usuarios.add(usuario);
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void resetRepository() {
		usuarios = new ArrayList<Usuario>();
	}

	public void reservarAsiento(Asiento asiento, Usuario loggedUser) throws BusinessException {
		Reserva reserva= loggedUser.reservar(asiento);
		reserva.setIdReserva(idProvider.getIdForReserva());
	}

}
