package application.DAOs.mongoDB;

import java.time.LocalDate;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import application.DAOs.LogConsultasInterface;
import application.business.Consulta;
import application.business.Usuario;
import application.models.SearchModel;
import application.utils.DateUtils;

@Repository
@ConditionalOnProperty(name="spring.configuration.use_search_logs")
@Primary
public class LogConsultasMorphia implements LogConsultasInterface {
	
	private @Autowired Datastore ds;
	
	public Object saveSearchLog(SearchModel model, Usuario loggedUser){
		Consulta consulta= new Consulta(loggedUser, model);
		return ds.save(consulta).getId();
	}
	
	public List<Consulta> getAllLogs(Usuario usuario) {
		Query<Consulta> query = ds.createQuery(Consulta.class);
		query.and(ds.createQuery(Consulta.class)
				.criteria("usuarioConsulta.username")
				.equal(usuario.getUsername()));
		
		return query.order("fechaHoraConsulta").asList();
	}

	public List<Consulta> searchLogsBetweenDates(LocalDate fechaDesde, LocalDate fechaHasta, Usuario usuario) {
		Query<Consulta> query = ds.createQuery(Consulta.class);
		query.and(ds.createQuery(Consulta.class)
				.criteria("usuarioConsulta.username")
				.equal(usuario.getUsername()));
			
			if(fechaDesde!=null)
				query.and(ds.createQuery(Consulta.class)
						.criteria("fechaHoraConsulta")
						.greaterThanOrEq(DateUtils.localDateToLocalDateTimeEarly(fechaDesde)));
			
			if(fechaHasta!=null)
				query.and(ds.createQuery(Consulta.class)
						.criteria("fechaHoraConsulta")
						.lessThanOrEq(DateUtils.localDateToLocalDateTimeLate(fechaHasta)));
					
		return query.order("fechaHoraConsulta").asList();
	}

	public Consulta getConsultaById(ObjectId idConsulta) {
		return ds.get(Consulta.class, idConsulta);
	}

	public int getCantidadLogs() {
		return ds.getCollection(Consulta.class).find().count();
	}

	public void deleteSearchLog(Object idLog) {
		ds.delete(Consulta.class, idLog);
	}
	
}
