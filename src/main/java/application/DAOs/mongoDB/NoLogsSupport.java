package application.DAOs.mongoDB;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import application.DAOs.LogConsultasInterface;
import application.business.Consulta;
import application.business.Usuario;
import application.models.SearchModel;

@Repository
public class NoLogsSupport implements LogConsultasInterface {

	protected final Logger log = LoggerFactory.getLogger(getClass());
	private final String MESSAGE_NO_LOGS= "No se guardan LOGs, (use_search_logs=false)";
	
	public Object saveSearchLog(SearchModel model, Usuario loggedUser) {
		return new Object();
	}

	public List<Consulta> getAllLogs(Usuario usuario) {
		log.info(MESSAGE_NO_LOGS);
		return new ArrayList<>();
	}

	public List<Consulta> searchLogsBetweenDates(LocalDate fechaDesde, LocalDate fechaHasta, Usuario usuario) {
		log.info(MESSAGE_NO_LOGS);
		return new ArrayList<>();
	}

	public Consulta getConsultaById(ObjectId idConsulta) {
		log.info(MESSAGE_NO_LOGS);
		return null;
	}

	public int getCantidadLogs() {
		return 0;
	}

	public void deleteSearchLog(Object idLog) {}
	
}
