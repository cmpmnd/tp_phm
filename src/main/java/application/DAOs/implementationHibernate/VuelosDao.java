package application.DAOs.implementationHibernate;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import application.DAOs.RepoVuelosInterface;
import application.DAOs.implementationHibernate.searchFilters.FiltroDestino;
import application.DAOs.implementationHibernate.searchFilters.FiltroOrigen;
import application.DAOs.implementationHibernate.searchFilters.FiltroPorFechaDesde;
import application.DAOs.implementationHibernate.searchFilters.FiltroPorFechaHasta;
import application.business.Aeropuerto;
import application.business.Asiento;
import application.business.Vuelo;
import application.constants.Profil;
import application.models.SearchModel;
import javassist.NotFoundException;

@Profile(Profil.HIBERNATE)
@Primary
@Repository
@Transactional
public class VuelosDao extends AbstractCommonHibernate implements RepoVuelosInterface {
	
	public Vuelo getVueloById(long vueloId) throws NotFoundException {
			Vuelo vueloEncontrado= (Vuelo) getSessionActual().createCriteria(Vuelo.class)
					.add( Restrictions.idEq(vueloId))
					.uniqueResult();
			
			if (vueloEncontrado == null)
				throw new NotFoundException("No se ha encontrado el vuelo solicitado");
			
			Hibernate.initialize(vueloEncontrado.getAsientos());
			return vueloEncontrado;
			
	}
	
	public void agregarVuelo(Vuelo vuelo) {
		getSessionActual().save(vuelo);
	}
	
	public void agregarAeropuerto(Aeropuerto aeropuerto) {
		getSessionActual().saveOrUpdate(aeropuerto);
	}
	
	public void updateSearchModel(SearchModel searchScreen) {
		Criteria criteria=(Criteria) getSessionActual().createCriteria(Vuelo.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		new FiltroOrigen(searchScreen, criteria);
		new FiltroDestino(searchScreen, criteria);
		new FiltroPorFechaDesde(searchScreen, criteria);
		new FiltroPorFechaHasta(searchScreen, criteria);
		
		List<Vuelo> vuelosFiltrados= castList(Vuelo.class, criteria.list());
		searchScreen.setVuelos(vuelosFiltrados);
		
		//Este filtro se debe aplicar luego de ejecutar la búsqueda;
		if (!searchScreen.getMonto().isEmpty())
			searchScreen.getVuelosModel().forEach(vm -> vm.setCantidadAsientosFiltrados(getCantidadAsientosLibres(searchScreen.getMonto(), vm.getIdVuelo())));
	}
	
	public int getCantidadAsientosLibres(String monto, long idVuelo) {
		List<Asiento> asientos= castList(Asiento.class, getSessionActual().createCriteria(Asiento.class)
				.add(Restrictions.eq("vueloPertenece.idVuelo", idVuelo))
				.add(Restrictions.and(Property.forName("estaReservado").eq(false)))
				//.add(Restrictions.lt("costoBase", Float.parseFloat(monto)))
				.list());
		return asientos.stream()
			.filter(asiento -> asiento.getCostoTotal()< Float.parseFloat(monto))
			.collect(Collectors.toList()).size();
	}
	
	public List<Vuelo> getVuelos() {
		List<Vuelo> vuelosEncontrados= castList(Vuelo.class, 
				getSessionActual()
				.createCriteria(Vuelo.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list()
			);
		vuelosEncontrados.forEach(vuelo -> Hibernate.initialize(vuelo.getAsientos()));
		return vuelosEncontrados;
	}
	
	public Set<Aeropuerto> getAeropuertos() {
		return new HashSet<Aeropuerto>(castList(Aeropuerto.class, getSessionActual().createCriteria(Aeropuerto.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list()));
	}
	
	public boolean existenVuelos() {
		long cantidadDeVuelosPersistidos= (Long)getSessionActual().createQuery("select count(*) from Vuelo").uniqueResult();
		log.info("Se detectaron " + cantidadDeVuelosPersistidos + " vuelos con HIBERNATE");
		return (cantidadDeVuelosPersistidos>0);
	}
	
}
