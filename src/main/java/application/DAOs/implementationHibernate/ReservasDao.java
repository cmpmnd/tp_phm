package application.DAOs.implementationHibernate;

import java.util.List;

import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import application.DAOs.RepoReservasInterface;
import application.business.Reserva;
import application.business.Usuario;
import application.constants.ErrorConstants;
import application.constants.Profil;
import javassist.NotFoundException;

@Profile(Profil.HIBERNATE)
@Primary
@Repository
@Transactional
public class ReservasDao extends AbstractCommonHibernate implements RepoReservasInterface {
	
	public List<Reserva> getReservasActivas(Usuario loggedUser) {
		//1) Native SQL Query
//		return castList(Reserva.class, 
//			getSessionActual().createSQLQuery("SELECT * FROM reserva WHERE fecha_cancelacion IS NULL and username=:userName")
//				.addEntity(Reserva.class)
//				.setParameter("userName", loggedUser.getUsername())
//				.list());
		
		//2) HQL (Hibernate Query Language)
//		return castList(Reserva.class, 
//			getSessionActual().createQuery("FROM Reserva WHERE fecha_cancelacion IS NULL and usuario_reserva_username=:userName")
//				.setParameter("userName", loggedUser.getUsername())
//				.list());
		
		//3) By Criteria
//		return castList(Reserva.class, 
//			getSessionActual().createCriteria(Reserva.class)
//				.add( Restrictions.and(Property.forName("usuarioReserva").eq(loggedUser)))
//			    .add( Restrictions.and(Property.forName("fechaCancelacion").isNull()))
//			    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
//			    .list());
		
		return loggedUser.getReservasActivas();
	}

	public void cancelarReserva(long idReserva, Usuario loggedUser) throws NotFoundException {
		Reserva reserva= (Reserva) getSessionActual().createCriteria(Reserva.class)
		.add( Restrictions.and(Property.forName("usuarioReserva").eq(loggedUser)))
	    .add( Restrictions.idEq(idReserva))
	    .uniqueResult();
		
		if (reserva==null)
			throw new NotFoundException(ErrorConstants.RESERVA_NO_ENCONTRADA);
		
		reserva.cancelarReserva();
		getSessionActual().update(reserva.getAsientoReservado());
		getSessionActual().update(reserva);
	}

}
