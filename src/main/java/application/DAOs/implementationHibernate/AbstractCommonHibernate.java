package application.DAOs.implementationHibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractCommonHibernate {
	protected final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	protected SessionFactory sessionFactory;
	
	protected Session getSessionActual() {
		return sessionFactory.getCurrentSession();
	}
	
	protected <T> List<T> castList(Class<? extends T> clazz, Collection<?> c) {
	    List<T> r = new ArrayList<T>(c.size());
	    for(Object o: c)
	      r.add(clazz.cast(o));
	    return r;
	}
}
