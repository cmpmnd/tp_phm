package application.DAOs.implementationHibernate.searchFilters;

import java.time.LocalDateTime;

import org.hibernate.Criteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import application.models.SearchModel;
import application.utils.DateUtils;

public class FiltroPorFechaHasta extends AbstractSearcher {

	public FiltroPorFechaHasta(SearchModel modelo, Criteria criteria) {
		super(modelo, criteria);
	}

	@Override
	public void search() {
		LocalDateTime fechaHasta= DateUtils.localDateToLocalDateTimeLate(modelo.getFechaHasta());
		criteria.add(Restrictions.and(Property.forName("fechaDespegue").le(fechaHasta)));
	}

	@Override
	public boolean deboFiltrar() {
		return modelo.getFechaHasta()!=null;
	}

}
