package application.DAOs.implementationHibernate.searchFilters;

import org.hibernate.Criteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import application.models.SearchModel;

public class FiltroDestino extends AbstractSearcher {

	public FiltroDestino(SearchModel modelo, Criteria criteria) {
		super(modelo, criteria);
	}

	@Override
	public void search() {
		criteria.add(Restrictions.and(Property.forName("destino.idAeropuerto").eq(modelo.getDestino())));
	}

	@Override
	public boolean deboFiltrar() {
		return modelo.getDestino()!=null;
	}

}
