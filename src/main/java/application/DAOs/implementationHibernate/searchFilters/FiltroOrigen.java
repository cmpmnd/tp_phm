package application.DAOs.implementationHibernate.searchFilters;

import org.hibernate.Criteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import application.models.SearchModel;

public class FiltroOrigen extends AbstractSearcher {

	public FiltroOrigen(SearchModel modelo, Criteria criteria) {
		super(modelo, criteria);
	}

	@Override
	public void search() {
		criteria.add(Restrictions.and(Property.forName("origen.idAeropuerto").eq(modelo.getOrigen())));
	}

	@Override
	public boolean deboFiltrar() {
		return modelo.getOrigen()!=null;
	}

}
