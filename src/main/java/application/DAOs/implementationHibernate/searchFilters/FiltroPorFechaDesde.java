package application.DAOs.implementationHibernate.searchFilters;

import java.time.LocalDateTime;

import org.hibernate.Criteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import application.models.SearchModel;
import application.utils.DateUtils;

public class FiltroPorFechaDesde extends AbstractSearcher {

	public FiltroPorFechaDesde(SearchModel modelo, Criteria criteria) {
		super(modelo, criteria);
	}

	@Override
	public void search() {
		LocalDateTime fechaDesde= DateUtils.localDateToLocalDateTimeEarly(modelo.getFechaDesde());
		criteria.add(Restrictions.and(Property.forName("fechaDespegue").ge(fechaDesde)));
	}

	@Override
	public boolean deboFiltrar() {
		return modelo.getFechaDesde()!=null;
	}

}
