package application.DAOs.implementationHibernate.searchFilters;

import org.hibernate.Criteria;

import application.models.SearchModel;

public abstract class AbstractSearcher {
	
	protected SearchModel modelo;
	protected Criteria criteria;
	
	public AbstractSearcher(SearchModel modelo, Criteria criteria){
		this.criteria = criteria;
		this.modelo = modelo;
		aplicarFiltros();
		
	}
	
	public abstract void search();
	
	public abstract boolean deboFiltrar();
	
	private void aplicarFiltros(){
		if(deboFiltrar())
			search();
	}
}
