package application.DAOs.implementationHibernate;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import application.DAOs.IdProviderInterface;
import application.constants.Profil;

@Profile({Profil.HIBERNATE, Profil.NEO4J})
@Primary
@Repository
public class IdGeneratorForJPA implements IdProviderInterface {

	private Long idNull=null;
	
	public Long getIdForVuelo() {
		return idNull;
	}
	public Long getIdForReserva() {
		return idNull;
	}
	public Long getIdForUsuario() {
		return idNull;
	}
	public Long getIdForAeropuerto() {
		return idNull;
	}
	public Long getIdForAsiento() {
		return idNull;
	}
	public Long getIdForTarifa() {
		return idNull;
	}
	public Long getIdForTramo() {
		return idNull;
	}
	
	public void resetAllIds() {}

}
