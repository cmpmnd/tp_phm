package application.DAOs.implementationHibernate;


import org.hibernate.Hibernate;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import application.DAOs.RepoUsuariosInterface;
import application.business.Asiento;
import application.business.Reserva;
import application.business.Usuario;
import application.constants.Profil;
import application.exceptions.BusinessException;

@Profile(Profil.HIBERNATE)
@Primary
@Repository
@Transactional
public class UsuariosDao extends AbstractCommonHibernate implements RepoUsuariosInterface {
	
	//Used by Spring Security
	public Usuario getUserByUsername(String username) {
		return (Usuario) getSessionActual().createCriteria(Usuario.class)
		.add(Restrictions.and(Property.forName("username").eq(username)))
		.uniqueResult();
	}
	
	public Usuario getUserById (long idUsuario) {
		Usuario usuario = getLazyUserById(idUsuario);
		Hibernate.initialize(usuario.getReservas());
		return usuario;
	}
	
	//Trae al usuario sin las reservas;
	public Usuario getLazyUserById(long idUsuario) {
		return (Usuario) getSessionActual().get(Usuario.class, idUsuario);
	}
	
	public void agregarUsuario(Usuario usuario) {
		getSessionActual().saveOrUpdate(usuario);
	}

	public void reservarAsiento(Asiento asiento, Usuario loggedUser) throws BusinessException {
		Reserva reserva = loggedUser.reservar(asiento);
		getSessionActual().merge(asiento);
		getSessionActual().persist(reserva);
	}

}
