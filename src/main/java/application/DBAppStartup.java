package application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import application.constants.Profil;
import application.services.DBInitializer;

@Component
@Profile({Profil.HIBERNATE, Profil.NEO4J})
@DependsOn("inMemoryAppStartup")
public class DBAppStartup implements ApplicationListener<ContextRefreshedEvent> {
	@Autowired private DBInitializer dbInitializer;

	@Override
	public void onApplicationEvent(final ContextRefreshedEvent event) {//Al comenzar la aplicacion:
		dbInitializer.executeInitialization();
	}
}