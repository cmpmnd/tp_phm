package application.configurations;

import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

import application.constants.Profil;

@Profile(Profil.HIBERNATE)
@Configuration
@Import(HibernateJpaAutoConfiguration.class)
public class HibernatePoolingConfig {

	@Bean
	public HibernateJpaSessionFactoryBean sessionFactory() {
		return new HibernateJpaSessionFactoryBean();
	}

}
