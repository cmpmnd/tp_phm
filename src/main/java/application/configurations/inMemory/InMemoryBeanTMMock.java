package application.configurations.inMemory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.PlatformTransactionManager;

import application.constants.Profil;

@Profile(Profil.INMEMORY)
@Configuration
public class InMemoryBeanTMMock {
	
	/* Se brinda una implementación PlatformTransactionManager para poder correr inMemory los tests
	 * transaccionales sin que se vean afectados por el manejo de transacción de SpringJUnit4ClassRunner
	*/
	
	@Bean
	public PlatformTransactionManager inMemoryMockTransactionManager() {
		return new InMemoryMockTransactionManager();
	}

}
