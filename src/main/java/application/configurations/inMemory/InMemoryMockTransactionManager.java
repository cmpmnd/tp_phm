package application.configurations.inMemory;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;

/**
 * Esta implementación se registra para poder correr los tests transaccionales en memoria.<br>
 * No tiene funcionamiento.
*/
public class InMemoryMockTransactionManager implements PlatformTransactionManager {

	@Override
	public TransactionStatus getTransaction(TransactionDefinition definition) throws TransactionException {
		return null;
	}

	@Override
	public void commit(TransactionStatus status) throws TransactionException {
		
	}

	@Override
	public void rollback(TransactionStatus status) throws TransactionException {
		
	}

}
