package application.configurations.converters.morphia;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import org.mongodb.morphia.converters.SimpleValueConverter;
import org.mongodb.morphia.converters.TypeConverter;
import org.mongodb.morphia.mapping.MappedField;

import application.utils.DateUtils;

public class MorphiaDateConverters extends TypeConverter implements SimpleValueConverter {

    public MorphiaDateConverters() {
    	this(LocalDate.class);
    }

    public MorphiaDateConverters(Class<LocalDate> class1) {
		super(class1);
	}

	@Override
    public Object decode(Class<?> targetClass, Object fromDBObject, MappedField optionalExtraInfo) {
        if (fromDBObject == null) {
            return null;
        }

        if (fromDBObject instanceof Date) {
        	return DateUtils.dateToLocalDate((Date)fromDBObject);
        }

        throw new IllegalArgumentException(String.format("Cannot decode object of class: %s", fromDBObject.getClass().getName()));
    }

    @Override
    public Object encode(Object value, MappedField optionalExtraInfo) {
        if (value == null) {
            return null;
        }

        if (value instanceof LocalDateTime) {
        	return Timestamp.valueOf((LocalDateTime)value);
        }
        
        if (value instanceof LocalDate) {
        	return DateUtils.localDateToDate((LocalDate)value);
        }

        throw new IllegalArgumentException(String.format("Cannot encode object of class: %s", value.getClass().getName()));
    }
}