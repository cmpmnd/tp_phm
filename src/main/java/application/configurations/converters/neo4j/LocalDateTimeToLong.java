package application.configurations.converters.neo4j;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.springframework.core.convert.converter.Converter;


public class LocalDateTimeToLong implements Converter<LocalDateTime, Long> {

	public Long convert(LocalDateTime dateTime) {
		return dateTime.toInstant(ZoneOffset.UTC).toEpochMilli();
	}
}
