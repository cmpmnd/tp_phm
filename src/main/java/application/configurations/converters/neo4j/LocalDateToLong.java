package application.configurations.converters.neo4j;

import java.time.LocalDate;
import java.time.ZoneOffset;

import org.springframework.core.convert.converter.Converter;


public class LocalDateToLong implements Converter<LocalDate, Long> {

	@Override
	public Long convert(LocalDate localDate) {
		return localDate.atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli();
	}

}
