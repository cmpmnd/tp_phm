package application.configurations.converters.neo4j;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.springframework.core.convert.converter.Converter;


public class LongToLocalDateTime implements Converter<Long, LocalDateTime> {

	public LocalDateTime convert(Long millis) {
		return LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneOffset.UTC);
	}

}
