package application.configurations;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.ConverterRegistry;
import org.springframework.data.neo4j.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.config.Neo4jConfiguration;

import application.configurations.converters.neo4j.LocalDateTimeToLong;
import application.configurations.converters.neo4j.LocalDateToLong;
import application.configurations.converters.neo4j.LongToLocalDate;
import application.configurations.converters.neo4j.LongToLocalDateTime;
import application.constants.Profil;

@Profile(Profil.NEO4J)
@Configuration
@EnableNeo4jRepositories
class Neo4JAppConfig extends Neo4jConfiguration {
	
	public Neo4JAppConfig() {
		setBasePackage("application.business");
	}
	
	@Bean
	GraphDatabaseService graphDatabaseService(@Value("${spring.data.neo4j.database.location}") String path) {
		return new GraphDatabaseFactory().newEmbeddedDatabase(path);
	}
	
    @Bean
    protected ConversionService neo4jConversionService() throws Exception {
        ConversionService conversionService = super.neo4jConversionService();
        ConverterRegistry registry = (ConverterRegistry) conversionService;

        registry.addConverter(new LocalDateToLong());
        registry.addConverter(new LongToLocalDate());
        
        registry.addConverter(new LocalDateTimeToLong());
        registry.addConverter(new LongToLocalDateTime());
        
        return conversionService;
    }
	
}
