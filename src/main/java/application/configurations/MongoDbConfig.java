package application.configurations;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.DatastoreImpl;
import org.mongodb.morphia.Morphia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mongodb.MongoClient;

import application.configurations.converters.morphia.MorphiaDateConverters;
import application.configurations.converters.morphia.MorphiaDateTimeConverters;

@Configuration
public class MongoDbConfig {

    @Bean
    @Autowired
    public Datastore datastore(MongoClient mongoClient, @Value("${spring.data.mongodb.database}") String nombreBase) {
    	Morphia morphia= new Morphia();
    	morphia.getMapper().getConverters().addConverter(MorphiaDateConverters.class);
    	morphia.getMapper().getConverters().addConverter(MorphiaDateTimeConverters.class);
    	return new DatastoreImpl(morphia, mongoClient, nombreBase);
    }
}
