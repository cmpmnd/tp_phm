package application.configurations.security;

import java.util.Collection;
import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import application.DAOs.RepoUsuariosInterface;
import application.business.Usuario;

@Service
public class CustomUserDetailsService implements UserDetailsService {
	
	@Autowired RepoUsuariosInterface userDao;
	
	private boolean enabled=true;
	private boolean accountNonExpired=true;
	private boolean credentialsNonExpired=true;
	private Collection<? extends GrantedAuthority> authorities= new LinkedList<>();
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario user = userDao.getUserByUsername(username);
		
		if (user == null) 
			throw new UsernameNotFoundException(username);
		
		return new OwnUserForSecurity(user.getIdUsuario(), 
				user.getUsername(), 
				user.getPassword(), 
				user.getNombre(), 
				user.getApellido(), 
				enabled, accountNonExpired, 
				credentialsNonExpired, credentialsNonExpired, 
				authorities);
	}

}
