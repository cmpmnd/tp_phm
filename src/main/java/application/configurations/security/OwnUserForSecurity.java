package application.configurations.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class OwnUserForSecurity extends User {

	private static final long serialVersionUID = 1L;
	private long idUsuario;
	private String nombre;
	private String apellido;

	public OwnUserForSecurity(long idUsuario, String username, String password, String nombre, String apellido,
			boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {

		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		
		this.setIdUsuario(idUsuario);
		this.setNombre(nombre);
		this.setApellido(apellido);
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombreYApellido() {
		return this.nombre + " " + this.apellido;
	}
}
