package application;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import application.exceptions.BusinessException;
import application.services.InMemoryInitializer;

@Component(value="inMemoryAppStartup")
public class InMemoryAppStartup implements ApplicationListener<ContextRefreshedEvent> {
	@Autowired private InMemoryInitializer localDataInitializer;

	@Override
	public void onApplicationEvent(final ContextRefreshedEvent event) {//Al comenzar la aplicacion:
		try {
			localDataInitializer.populate();
		} catch (BusinessException e) {
			LoggerFactory.getLogger(getClass()).error(e.getMessage());
		}
	}
}