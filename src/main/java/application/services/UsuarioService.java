package application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import application.DAOs.RepoUsuariosInterface;
import application.business.Asiento;
import application.business.Usuario;
import application.exceptions.BusinessException;

@Service
public class UsuarioService {
	@Autowired private RepoUsuariosInterface repoUsuarios;
	
	public Usuario getUserById(long idUsuario) {
		return repoUsuarios.getUserById(idUsuario);
	}
	
	public void agregarUsuario(Usuario usuario) {
		repoUsuarios.agregarUsuario(usuario);
	}

	public void reservarAsiento(Asiento asiento, Usuario loggedUser) throws BusinessException {
		repoUsuarios.reservarAsiento(asiento, loggedUser);
	}

	public Usuario getUserByUsername(String username) {
		return repoUsuarios.getUserByUsername(username);
	}
}
