package application.services;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import application.DAOs.RepoVuelosInterface;
import application.business.Aeropuerto;
import application.business.Vuelo;
import application.models.SearchModel;
import javassist.NotFoundException;

@Service
public class VueloService {
	@Autowired private RepoVuelosInterface repoVuelos;
	
	public List<Vuelo> getVuelos() {
		return repoVuelos.getVuelos();
	}
	
	public Vuelo getVueloById(long vueloId) throws NotFoundException,Exception {
		return repoVuelos.getVueloById(vueloId);
	}
	
	public Set<Aeropuerto> getAeropuertos() {
		return repoVuelos.getAeropuertos();
	}
	
	public void searchVuelosWithModel(SearchModel searchModel){
		repoVuelos.updateSearchModel(searchModel);
	}
	
	//Used by JUnit Tests;
	public void agregarVuelo(Vuelo vuelo) {
		repoVuelos.agregarVuelo(vuelo);
	}
}
