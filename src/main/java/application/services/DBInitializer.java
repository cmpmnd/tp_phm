package application.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import application.DAOs.RepoUsuariosInterface;
import application.DAOs.RepoVuelosInterface;
import application.DAOs.implementationInMemory.RepoUsuarios;
import application.DAOs.implementationInMemory.RepoVuelos;
import application.constants.Profil;

@Service
@Profile({Profil.HIBERNATE, Profil.NEO4J})
public class DBInitializer {
	protected final Logger log = LoggerFactory.getLogger(getClass());
	
	//inMemory
	@Autowired private RepoUsuarios repoUsuarios;
	@Autowired private RepoVuelos repoVuelos;
	
	//dataBase
	@Autowired RepoUsuariosInterface usuariosDao;
	@Autowired RepoVuelosInterface vuelosDao;
	
	private boolean needDbPopulation() {
		return !vuelosDao.existenVuelos();
	}
	
	public void executeInitialization() {
		if (needDbPopulation())
			populateDB();
	}

	private void populateDB() {
		log.info("Se comienza la inicializacion de datos de la base");
		
		// Aeropuertos -> Vuelos con Asientos y Tramos -> Usuario y Reserva
		repoVuelos.getAeropuertos().forEach(aeropuerto -> vuelosDao.agregarAeropuerto(aeropuerto));
		repoVuelos.getVuelos().forEach(vuelo -> vuelosDao.agregarVuelo(vuelo));
		repoUsuarios.getUsuarios().forEach(usuario-> usuariosDao.agregarUsuario(usuario));
		
		log.info("Juego de datos generado con éxito");
	}
	
}
