package application.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import application.DAOs.RepoReservasInterface;
import application.DAOs.RepoUsuariosInterface;
import application.business.Asiento;
import application.business.Reserva;
import application.business.Usuario;
import application.exceptions.BusinessException;
import javassist.NotFoundException;

@Service
public class ReservasService {
	@Autowired private RepoReservasInterface repoReservas;
	@Autowired private RepoUsuariosInterface repoUsuarios;
	
	public List<Reserva> getReservasActivas(long idUsuarioLogeado) {
		return repoReservas.getReservasActivas(getUsuario(idUsuarioLogeado));
	}

	public void cancelarReserva(long idReserva, long idUsuarioLogeado) throws NotFoundException {
		repoReservas.cancelarReserva(idReserva, getLazyUsuario(idUsuarioLogeado));
	}

	public void reservarAsiento(Asiento asiento, long idUsuarioLogeado) throws BusinessException {
		repoUsuarios.reservarAsiento(asiento, getUsuario(idUsuarioLogeado));
	}
	
	private Usuario getUsuario(long idUsuario) {
		return repoUsuarios.getUserById(idUsuario);
	}
	
	private Usuario getLazyUsuario(long idUsuario) {
		return repoUsuarios.getLazyUserById(idUsuario);
	}

}
