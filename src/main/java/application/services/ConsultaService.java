package application.services;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import application.DAOs.LogConsultasInterface;
import application.DAOs.RepoUsuariosInterface;
import application.business.Consulta;
import application.business.Usuario;
import application.models.LogsModel;
import application.models.SearchModel;

@Service
public class ConsultaService {
	@Autowired private LogConsultasInterface logConsultas;
	@Autowired private RepoUsuariosInterface repoUsuariosInterface;
	
	public Object saveSearchLog(SearchModel model, long loggedUser){
		return logConsultas.saveSearchLog(model, getUserById(loggedUser));
	}

	public List<Consulta> getAllLogs(long loggedUser) {
		return logConsultas.getAllLogs(getUserById(loggedUser));
	}
	
	public List<Consulta> searchLogsBetweenDates(LogsModel logsModel, long loggedUser) {
		return logConsultas.searchLogsBetweenDates(logsModel.getFechaDesde(), logsModel.getFechaHasta(), getUserById(loggedUser));
	}
	
	private Usuario getUserById(long loggedUser) {
		return repoUsuariosInterface.getUserById(loggedUser);
	}

	public Consulta getConsultaById(ObjectId idConsulta) {
		return logConsultas.getConsultaById(idConsulta);
	}

	public int getCantidadLogs() {
		return logConsultas.getCantidadLogs();
	}
	
	public void deleteSearchLog(Object idLog){
		logConsultas.deleteSearchLog(idLog);
	}

}
