package application.services;

import java.time.LocalDateTime;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import application.DAOs.IdProviderInterface;
import application.DAOs.implementationInMemory.RepoUsuarios;
import application.DAOs.implementationInMemory.RepoVuelos;
import application.business.Aeropuerto;
import application.business.Asiento;
import application.business.Asiento.Ubicacion;
import application.business.Tramo;
import application.business.Usuario;
import application.business.Vuelo;
import application.business.tarifaAsientos.Tarifa;
import application.business.tarifaAsientos.TarifaBandaNegativa;
import application.business.tarifaAsientos.TarifaComun;
import application.business.tarifaAsientos.TarifaEspecial;
import application.constants.CiudadConstants;
import application.exceptions.BusinessException;

@Service
public class InMemoryInitializer {

	@Autowired private RepoUsuarios repoUsuarios;
	@Autowired private RepoVuelos repoVuelos;
	@Autowired private IdProviderInterface idProvider;
	
	private final int CANTIDAD_RESERVAS_LEANDRO=7;
	private final int CANTIDAD_RESERVAS_JUAN=13;
	private final int CANTIDAD_RESERVAS_DANIEL=2;
	private final int CANTIDAD_RESERVAS_DODAIN=11;
	
	public void cleanAndPopulate() throws BusinessException {
		repoUsuarios.resetRepository();
		repoVuelos.resetRepository();
		populate();
	}
	
	public void populate() throws BusinessException {
		//----USUARIOS----
		BCryptPasswordEncoder encoder= new BCryptPasswordEncoder();
		Usuario leandro = new Usuario(idProvider.getIdForUsuario(), "salo", encoder.encode("fumanchu"), "Leandro", "Salo");
		Usuario juan = new Usuario(idProvider.getIdForUsuario(), "juan", encoder.encode("admin"), "Juan", "Petelski");
		Usuario daniel = new Usuario(idProvider.getIdForUsuario(), "daniel", encoder.encode("daniel"), "Daniel", "Balboa");
		Usuario dodino = new Usuario(idProvider.getIdForUsuario(), "dodain", encoder.encode("dodain"), "Fernando", "Dodino");

		//----AEROPUERTOS----
		Aeropuerto aeropuertoBsAs= new Aeropuerto(idProvider.getIdForAeropuerto(), CiudadConstants.BUENOS_AIRES);
		Aeropuerto aeropuertoTucuman= new Aeropuerto(idProvider.getIdForAeropuerto(), CiudadConstants.TUCUMAN);
		Aeropuerto aeropuertoSaoPablo= new Aeropuerto(idProvider.getIdForAeropuerto(), CiudadConstants.SAO_PABLO);
		Aeropuerto aeropuertoChile= new Aeropuerto(idProvider.getIdForAeropuerto(), CiudadConstants.CHILE);
		Aeropuerto aeropuertoCanada= new Aeropuerto(idProvider.getIdForAeropuerto(), CiudadConstants.CANADA);
		Aeropuerto aeropuertoEntraBsAsCanada= new Aeropuerto(idProvider.getIdForAeropuerto(), CiudadConstants.MEXICO);
		
		//----TRAMOS----
		Tramo tramoEntre_Cad_BsAs = new Tramo(
				idProvider.getIdForTramo(),
				LocalDateTime.of(2016, 12, 12, 12, 10),
				LocalDateTime.of(2016, 12, 12, 12, 30),
				aeropuertoEntraBsAsCanada);
		
		Tramo tramoEntre_BsAs_Cad = new Tramo(
				idProvider.getIdForTramo(),
				LocalDateTime.of(2016, 12, 13, 11, 15),
				LocalDateTime.of(2016, 12, 13, 11, 35),
				aeropuertoEntraBsAsCanada);
				
		//----VUELOS----
		Vuelo vuelo_BsAs_Tucuman = new Vuelo.VueloBuilder()
				.idVuelo(idProvider.getIdForVuelo())
				.aerolinea("Aerolineas Argentinas")
				.origen(aeropuertoBsAs)
				.destino(aeropuertoTucuman)
				.fechaDespegue(LocalDateTime.of(2016, 05, 21, 10, 40))
				.fechaAterrizaje(LocalDateTime.of(2016, 05, 22, 02, 17))
			.build();

		Vuelo vuelo_SaoPablo_Chile = new Vuelo.VueloBuilder()
				.idVuelo(idProvider.getIdForVuelo())
				.aerolinea("LAN")
				.origen(aeropuertoSaoPablo)
				.destino(aeropuertoChile)
				.fechaDespegue(LocalDateTime.of(2016, 8, 15, 7, 25))
				.fechaAterrizaje(LocalDateTime.of(2016, 8, 15, 18, 40))
			.build();

		Vuelo vuelo_Chile_SaoPablo = new Vuelo.VueloBuilder()
				.idVuelo(idProvider.getIdForVuelo())
				.aerolinea("LAN")
				.origen(aeropuertoChile)
				.destino(aeropuertoSaoPablo)
				.fechaDespegue(LocalDateTime.of(2016, 8, 15, 7, 25))
				.fechaAterrizaje(LocalDateTime.of(2016, 8, 15, 18, 40))
			.build();
		
		Vuelo vuelo_Canada_BsAs = new Vuelo.VueloBuilder()
				.idVuelo(idProvider.getIdForVuelo())
				.aerolinea("LAN")
				.origen(aeropuertoCanada)
				.destino(aeropuertoBsAs)
				.fechaDespegue(LocalDateTime.of(2016, 12, 12, 9, 25))
				.fechaAterrizaje(LocalDateTime.of(2016, 12, 12, 15, 37))
				.agregarTramo(tramoEntre_Cad_BsAs)
			.build();
		
		Vuelo vuelo_BsAs_Canada = new Vuelo.VueloBuilder()
				.idVuelo(idProvider.getIdForVuelo())
				.aerolinea("LAN")
				.origen(aeropuertoBsAs)
				.destino(aeropuertoCanada)
				.fechaDespegue(LocalDateTime.of(2016, 12, 13, 7, 15))
				.fechaAterrizaje(LocalDateTime.of(2016, 12, 13, 13, 20))
				.agregarTramo(tramoEntre_BsAs_Cad)
			.build();
		
		
		//----ASIENTOS CON TARIFAS----
		generateAsientosForVuelo(vuelo_BsAs_Tucuman);
		generateAsientosForVuelo(vuelo_SaoPablo_Chile);
		generateAsientosForVuelo(vuelo_Chile_SaoPablo);
		generateAsientosForVuelo(vuelo_Canada_BsAs);
		generateAsientosForVuelo(vuelo_BsAs_Canada);

		
		
		//----POPULATE----
		repoVuelos.agregarVuelo(vuelo_BsAs_Tucuman);
		repoVuelos.agregarVuelo(vuelo_SaoPablo_Chile);
		repoVuelos.agregarVuelo(vuelo_Chile_SaoPablo);
		repoVuelos.agregarVuelo(vuelo_Canada_BsAs);
		repoVuelos.agregarVuelo(vuelo_BsAs_Canada);
		repoUsuarios.agregarUsuario(daniel);
		repoUsuarios.agregarUsuario(dodino);
		repoUsuarios.agregarUsuario(leandro);
		repoUsuarios.agregarUsuario(juan);
		
		
		//----RESERVAS----
		for (int i=0; i<CANTIDAD_RESERVAS_DANIEL; i++)
			daniel.reservar(getAsientoLibreFromVuelo(getVueloAleatorio())).setIdReserva(idProvider.getIdForReserva());
		
		for (int i=0; i<CANTIDAD_RESERVAS_LEANDRO; i++)
			leandro.reservar(getAsientoLibreFromVuelo(getVueloAleatorio())).setIdReserva(idProvider.getIdForReserva());
		
		for (int i=0; i<CANTIDAD_RESERVAS_JUAN; i++)
			juan.reservar(getAsientoLibreFromVuelo(getVueloAleatorio())).setIdReserva(idProvider.getIdForReserva());
		
		for (int i=0; i<CANTIDAD_RESERVAS_DODAIN; i++)
			dodino.reservar(getAsientoLibreFromVuelo(getVueloAleatorio())).setIdReserva(idProvider.getIdForReserva());
		
		
	}
	
	private void generateAsientosForVuelo(Vuelo vuelo) {
		for (int i=1; i<=10; i++) {
			generateAsientoWith(i, vuelo, Ubicacion.PASILLO, generateTarifaRandom(getRandom(6000, 13000)));
			generateAsientoWith(i, vuelo, Ubicacion.CENTRO, generateTarifaRandom(getRandom(5000, 12000)));
			generateAsientoWith(i, vuelo, Ubicacion.VENTANILLA, generateTarifaRandom(getRandom(10000, 20000)));
		}
	}
	
	private void generateAsientoWith(int position, Vuelo vuelo, Ubicacion ubic, Tarifa tarifa) {
		new Asiento.AsientoBuilder()
			.idAsiento(idProvider.getIdForAsiento())
			.fila(position)
			.ubicacion(ubic)
			.tarifa(tarifa)
			.vueloPertenece(vuelo)
		.build();
	}
	
	private Tarifa generateTarifaRandom(Float costoRandom) {
		float randomNumber= new Random().nextInt(3)+1;
		if (randomNumber==1)
			return new TarifaComun(idProvider.getIdForTarifa(), costoRandom);
		if (randomNumber==2)
			return new TarifaBandaNegativa(idProvider.getIdForTarifa(), costoRandom);
		
		return new TarifaEspecial(idProvider.getIdForTarifa(), costoRandom, getRandom(1000, 3000));
	}
	
	private Vuelo getVueloAleatorio() {
		final int INITIAL_POSITION= 0;
		float maxValue=repoVuelos.getVuelos().size();
		return repoVuelos.getVuelos().get((int) getRandom(INITIAL_POSITION, maxValue));
	}
	
	private float getRandom(float desde, float hasta) {
	    return desde + new Random().nextFloat() * (hasta - desde);
	}
	
	private Asiento getAsientoLibreFromVuelo(Vuelo vuelo) {
		return vuelo.getAsientos().stream()
				.filter(asiento -> !asiento.isEstaReservado())
				.findAny()
				.get();
	}
}
