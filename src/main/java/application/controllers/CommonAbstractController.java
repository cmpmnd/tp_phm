package application.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import application.configurations.security.OwnUserForSecurity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class CommonAbstractController {
	
	protected final Logger log = LoggerFactory.getLogger(getClass());
	
    protected long getIdUsuarioLogeado() {
    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	OwnUserForSecurity loggedUser = (OwnUserForSecurity)authentication.getPrincipal();
    	return loggedUser.getIdUsuario();
    }
}