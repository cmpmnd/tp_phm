package application.controllers;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import application.models.LogsModel;
import application.services.ConsultaService;


@Controller
public class LogsController extends CommonAbstractController{
	@Autowired ConsultaService consultaService;
	
	@RequestMapping(value="/showLogScreen", method=RequestMethod.GET)
    public String showLogScreen(LogsModel logsModel) {
		logsModel.setConsultas(consultaService.getAllLogs(getIdUsuarioLogeado()));
        return "historyLogScreen";
    }
	
	@RequestMapping(value="/filtrarLogs", method=RequestMethod.POST)
    public String filterLogs(@Valid LogsModel logsModel, BindingResult bindingResult) {
		if (!bindingResult.hasErrors())
			logsModel.setConsultas(consultaService.searchLogsBetweenDates(logsModel, getIdUsuarioLogeado()));
        return "historyLogScreen";
    }	
	
	@RequestMapping(value="/showFlightLogScreen/{idConsulta}", method=RequestMethod.GET)
    public String showFlightLogScreen(@PathVariable ObjectId idConsulta, Model model) {
		model.addAttribute("consulta", consultaService.getConsultaById(idConsulta));
        return "flightLogScreen";
    }
}