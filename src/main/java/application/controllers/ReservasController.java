package application.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import application.business.Asiento;
import application.exceptions.BusinessException;
import application.services.ReservasService;
import application.services.VueloService;
import javassist.NotFoundException;


@Controller
@SessionAttributes("searchModel")
public class ReservasController extends CommonAbstractController{
	@Autowired private ReservasService reservasService;
	@Autowired private VueloService vuelosService;
	
	
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String redirectMainScreen() {
        return "redirect:/showMainScreen";
    }
    
    @RequestMapping(value="/showMainScreen", method=RequestMethod.GET)
    public String showMainScreen(Model model, SessionStatus status) {
    	model.addAttribute("reservas", reservasService.getReservasActivas(getIdUsuarioLogeado()));
    	status.setComplete();
        return "mainScreen";
    }
    
	@RequestMapping(value = "/cancelarReserva/{idReserva}", method = RequestMethod.GET)
	public String cancelarReserva(@PathVariable long idReserva, RedirectAttributes redirectAttributes) {
		try {
			reservasService.cancelarReserva(idReserva, getIdUsuarioLogeado());
			redirectAttributes.addFlashAttribute("message", "La reserva fue cancelada");
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		}
		
		return "redirect:/showMainScreen";
	}
    
	
    @RequestMapping(value="/showBookScreen/{vueloId}", method=RequestMethod.GET)
    public String showBookScreen(@PathVariable long vueloId, Model model, RedirectAttributes redirectAttributes) {
    	try {
			model.addAttribute("vueloModel", vuelosService.getVueloById(vueloId));
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
			return "redirect:/showSearchScreen";
		}
        return "bookScreen";
    }
    
    @RequestMapping(value="/reservarAsiento/{vueloId}/{posicionAsiento}", method=RequestMethod.GET)
    public String reservarAsiento(@PathVariable long vueloId, @PathVariable String posicionAsiento, RedirectAttributes redirectAttributes) {
    	Asiento asientoParaReserva = null;
		try {
			asientoParaReserva = vuelosService.getVueloById(vueloId).getAsiento(posicionAsiento);
		} catch (Exception e1) {
			redirectAttributes.addFlashAttribute("error", e1.getMessage());
		}
    	try {
			reservasService.reservarAsiento(asientoParaReserva, getIdUsuarioLogeado());
			redirectAttributes.addFlashAttribute("message", "El asiento " + posicionAsiento + " fue reservado");
		} catch (BusinessException e) {
			log.error(e.getMessage());
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		}
        return "redirect:/showBookScreen/" + vueloId;
    }
	
}