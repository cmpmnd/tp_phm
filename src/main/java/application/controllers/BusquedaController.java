package application.controllers;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import application.models.SearchModel;
import application.services.ConsultaService;
import application.services.VueloService;


@Controller
@SessionAttributes("searchModel")

public class BusquedaController extends CommonAbstractController{
	@Autowired private VueloService vuelosService;
	@Autowired private ConsultaService consultaService;
	
    @RequestMapping(value="/showSearchScreen", method=RequestMethod.GET)
    public String showSearchScreen(HttpSession session, Model model, RedirectAttributes redirectAttributes) {
    	SearchModel searchModel= (SearchModel)session.getAttribute("searchModel");
    	if (searchModel==null) {
    		searchModel= new SearchModel();
    		searchModel.setVuelos(vuelosService.getVuelos());
    		searchModel.setAeropuertos(vuelosService.getAeropuertos());
    	} else {
			vuelosService.searchVuelosWithModel(searchModel);
    	}
    	model.addAttribute("searchModel", searchModel);
    	
        return "searchScreen";
    }
    
    @RequestMapping(value="/search", method=RequestMethod.POST)
    public String showResultsScreen(@Valid SearchModel searchModel, BindingResult bindingResult, RedirectAttributes redirectAttributes){
    	searchModel.setAeropuertos(vuelosService.getAeropuertos());
    	
    	if (!bindingResult.hasErrors()) {//Si no hay errores
        	searchModel.setVuelos(vuelosService.getVuelos());
			vuelosService.searchVuelosWithModel(searchModel);
			try {
				consultaService.saveSearchLog(searchModel, getIdUsuarioLogeado());
			} catch (NoSuchElementException e) {
				redirectAttributes.addFlashAttribute("error", e.getMessage());
				return "redirect:/showSearchScreen";
			}
    	} else {//Si hay errores
    		searchModel.setVuelos(new ArrayList<>());
    	}
    	
		return "searchScreen";
    }
}