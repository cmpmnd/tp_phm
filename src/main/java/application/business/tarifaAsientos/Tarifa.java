package application.business.tarifaAsientos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;

import application.business.Asiento;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NodeEntity
public abstract class Tarifa {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@GraphId
	protected Long idTarifa;
	
	protected Float costoBase;

	protected Tarifa() {}
	
	public Tarifa(Long idTarifa, Float costo) {
		this.idTarifa = idTarifa;
		this.costoBase= costo;
	}
	
	public abstract Float calcularCosto(Asiento asiento);
}
