package application.business.tarifaAsientos;

import javax.persistence.Entity;

import application.business.Asiento;

@Entity
public class TarifaEspecial extends Tarifa {
	
	private Float descuento;
	
	public TarifaEspecial(Long idTarifa, Float costoBase, Float descuento) {
		super(idTarifa, costoBase);
		this.descuento= descuento;
	}
	
	protected TarifaEspecial() {}

	@Override
	public Float calcularCosto(Asiento asiento) {
		return costoBase - descuento;
	}

}
