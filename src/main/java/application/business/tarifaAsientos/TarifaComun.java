package application.business.tarifaAsientos;

import javax.persistence.Entity;

import application.business.Asiento;

@Entity
public class TarifaComun extends Tarifa {

	public TarifaComun(Long idTarifa, Float costo) {
		super(idTarifa, costo);
	}

	public TarifaComun() {}
	
	@Override
	public Float calcularCosto(Asiento asiento) {
		return costoBase;
	}
}
