package application.business.tarifaAsientos;

import java.time.LocalDateTime;

import javax.persistence.Entity;

import application.business.Asiento;

@Entity
public class TarifaBandaNegativa extends Tarifa {

	public TarifaBandaNegativa(Long idTarifa, Float costo) {
		super(idTarifa, costo);
	}
	
	protected TarifaBandaNegativa(){}

	@Override
	public Float calcularCosto(Asiento asiento) {
		if (faltanDiasParaElVuelo(asiento, 3))
			return aplicarPorcientoDescuento(20);
		return aplicarPorcientoDescuento(10);
	}

	private Float aplicarPorcientoDescuento(int porcentajeDescuento) {
		return costoBase*(1 - (float)porcentajeDescuento/100);
	}

	private boolean faltanDiasParaElVuelo(Asiento asiento, int diasFaltantes) {
		LocalDateTime fechaDespegue= asiento.getVueloPertenece().getFechaDespegue();
		LocalDateTime fechaLimite= fechaDespegue.minusDays(diasFaltantes);
		
		return LocalDateTime.now().isAfter(fechaLimite) && LocalDateTime.now().isBefore(fechaDespegue);
	}

}
