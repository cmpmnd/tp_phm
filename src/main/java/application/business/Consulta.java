package application.business;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import application.models.SearchModel;
import application.models.VueloModel;
import application.utils.DateUtils;

@Entity
public class Consulta {
	@Id
	private ObjectId idConsulta;
	
	private Usuario usuarioConsulta;
	private LocalDateTime fechaHoraConsulta;

	private Aeropuerto filtroDestino;
	private Aeropuerto filtroOrigen;
	private LocalDate filtroFechaDesde;
	private LocalDate filtroFechaHasta;
	private String filtroMonto;
	
	private List<VueloModel> vuelosEncontrados = new ArrayList<>();
	
	protected Consulta() {}
	
	public Consulta(Usuario usuarioConsulta, SearchModel searchModel) {
		this.usuarioConsulta = usuarioConsulta;
		this.fechaHoraConsulta = LocalDateTime.now();
		
		populateConsulta(searchModel);
	}

	private void populateConsulta(SearchModel searchModel) {
		this.filtroDestino = searchModel.getAeropuertoDestino();
		this.filtroOrigen = searchModel.getAeropuertoOrigen();
		this.filtroFechaDesde = searchModel.getFechaDesde();
		this.filtroFechaHasta = searchModel.getFechaHasta();
		this.filtroMonto = searchModel.getMonto();
		this.vuelosEncontrados = searchModel.getVuelosModel();
	}

	public boolean isNullSearched() {
		return (filtroOrigen==null && filtroDestino==null && filtroFechaDesde==null && filtroFechaHasta==null && filtroMonto.isEmpty());
	}
	
	public int getCantidadVuelosEncontrados() {
		return vuelosEncontrados.size();
	}
	
	public boolean isVuelosEmpty(){
		return vuelosEncontrados.isEmpty();
	}

	public String getDescFechaHoraConsulta() {
		return DateUtils.dateTimeToString(fechaHoraConsulta);
	}
	
	public String getDescFiltroOrigen() {
		return filtroOrigen.getCiudad();
	}
	
	public String getDescFiltroDestino() {
		return filtroDestino.getCiudad();
	}
	
	//Getters & Setters
	public ObjectId getIdConsulta() {
		return idConsulta;
	}
	
	public void setIdConsulta(ObjectId idConsulta) {
		this.idConsulta = idConsulta;
	}
	
	public Usuario getUsuarioConsulta() {
		return usuarioConsulta;
	}

	public void setUsuarioConsulta(Usuario usuarioConsulta) {
		this.usuarioConsulta = usuarioConsulta;
	}

	public LocalDateTime getFechaHoraConsulta() {
		return fechaHoraConsulta;
	}

	public void setFechaHoraConsulta(LocalDateTime fechaHoraConsulta) {
		this.fechaHoraConsulta = fechaHoraConsulta;
	}

	public Aeropuerto getFiltroDestino() {
		return filtroDestino;
	}

	public void setFiltroDestino(Aeropuerto filtroDestino) {
		this.filtroDestino = filtroDestino;
	}

	public Aeropuerto getFiltroOrigen() {
		return filtroOrigen;
	}

	public void setFiltroOrigen(Aeropuerto filtroOrigen) {
		this.filtroOrigen = filtroOrigen;
	}

	public LocalDate getFiltroFechaDesde() {
		return filtroFechaDesde;
	}

	public void setFiltroFechaDesde(LocalDate filtroFechaDesde) {
		this.filtroFechaDesde = filtroFechaDesde;
	}

	public LocalDate getFiltroFechaHasta() {
		return filtroFechaHasta;
	}

	public void setFiltroFechaHasta(LocalDate filtroFechaHasta) {
		this.filtroFechaHasta = filtroFechaHasta;
	}

	public String getFiltroMonto() {
		return filtroMonto;
	}

	public void setFiltroMonto(String filtroMonto) {
		this.filtroMonto = filtroMonto;
	}

	public List<VueloModel> getVuelosEncontrados() {
		return vuelosEncontrados;
	}

	public void setVuelosEncontrados(List<VueloModel> vuelosEncontrados) {
		this.vuelosEncontrados = vuelosEncontrados;
	}
	
}
