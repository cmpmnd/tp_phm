package application.business;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import application.business.tarifaAsientos.Tarifa;

@Entity
@NodeEntity
public class Asiento implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@GraphId
	private Long id;
	
	@Transient
	private String desc; //Para visualizacion Neo4J
	
	private int fila;
	private Ubicacion ubicacion;
	private boolean estaReservado=false;
	
	@OneToOne(cascade = CascadeType.ALL)
	@RelatedTo(type="TARIFA")
	@Fetch
	private Tarifa tarifa;
	
	@OneToOne
	@JoinColumn(name="idVuelo")
	@RelatedTo(type="ASIENTO", direction=Direction.INCOMING)
	@Fetch
	private Vuelo vueloPertenece;

	private Asiento(AsientoBuilder builder) {
		this.id = builder.idAsiento;
		this.fila = builder.fila;
		this.ubicacion = builder.ubicacion;
		this.tarifa = builder.tarifa;
		this.vueloPertenece = builder.vueloPertenece;
		this.vueloPertenece.getAsientos().add(this);
		this.desc = getKey();
	}
	
	protected Asiento() {}

	public enum Ubicacion {
		PASILLO, CENTRO, VENTANILLA
	}
	
	public void reservar() {
		this.estaReservado = true;
	}
	
	public void cancelarReserva() {
		this.estaReservado = false;
	}
	
	public String getCiudadOrigen() {
		return vueloPertenece.getOrigen().getCiudad();
	}
	public String getCiudadDestino() {
		return vueloPertenece.getDestino().getCiudad();
	}
	public int getCantidadTramos() {
		return vueloPertenece.getCantidadTramos();
	}

	public LocalDateTime getFechaSalida() {
		return vueloPertenece.getFechaDespegue();
	}
	
	public LocalDateTime getFechaLlegada() {
		return vueloPertenece.getFechaAterrizaje();
	}
	
	//Getters & Setters
	public Long getId() {
		return id;
	}
	public int getFila() {
		return fila;
	}
	public Ubicacion getUbicacion() {
		return ubicacion;
	}
	public boolean isEstaReservado() {
		return estaReservado;
	}
	public Float getCostoTotal() {
		return tarifa.calcularCosto(this);
	}
	public Vuelo getVueloPertenece() {
		return vueloPertenece;
	}

	public Tarifa getTarifa() {
		return tarifa;
	}

	public void setTarifa(Tarifa tarifa) {
		this.tarifa = tarifa;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public String getKey() {
		if (ubicacion!=null)
			return "" + fila + getUbicacion().toString().charAt(0);
		return "error";
	}
	
	public String toString() {
		return getKey();
	}


	public static class AsientoBuilder {
		private Long idAsiento;
		private Vuelo vueloPertenece;
		private int fila;
		private Ubicacion ubicacion;
		private Tarifa tarifa;

		public AsientoBuilder fila(int fila) {
			if (fila<1 || fila>10)
				throw new IndexOutOfBoundsException();
			this.fila = fila;
			return this;
		}

		public AsientoBuilder idAsiento(Long idAsiento) {
			this.idAsiento = idAsiento;
			return this;
		}
		
		public AsientoBuilder ubicacion(Ubicacion ubicacion) {
			this.ubicacion = ubicacion;
			return this;
		}
		
		public AsientoBuilder tarifa(Tarifa tarifa) {
			this.tarifa = tarifa;
			return this;
		}
		
		public AsientoBuilder vueloPertenece(Vuelo vueloPertenece) {
			this.vueloPertenece = vueloPertenece;
			return this;
		}

		public Asiento build() {
			return new Asiento(this);
		}

	}
}
