package application.business;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.mongodb.morphia.annotations.Transient;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedToVia;

import application.constants.ErrorConstants;
import application.exceptions.BusinessException;
import javassist.NotFoundException;

@Entity
@org.mongodb.morphia.annotations.Entity
@NodeEntity
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@org.mongodb.morphia.annotations.Id
	@GraphId
	private Long idUsuario;
	
	@Column(length= 30, unique= true)
	private String username;
	
	@Column(length = 100)
	@Transient
	private String password;
	
	@Column(length = 50)
	private String nombre;
	
	@Column(length = 50)
	private String apellido;
	
	@OneToMany(mappedBy="usuarioReserva", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@Transient
	@RelatedToVia(type="RESERVO")
	private Set<Reserva> reservas = new HashSet<>();
	
	
	public Usuario(Long idUsuario, String username, String password, String nombre, String apellido) {
		this.idUsuario = idUsuario;
		this.username = username;
		this.password = password;
		this.nombre = nombre;
		this.apellido = apellido;
	}

	public Usuario(){}

	public Reserva reservar(Asiento asiento) throws BusinessException {
		if (asiento.isEstaReservado())
			throw new BusinessException(ErrorConstants.ASIENTO_YA_RESERVADO);
		Reserva reservaGenerada= new Reserva(this, asiento);
		reservas.add(reservaGenerada);
		return reservaGenerada;
	}

	public void cancelarReserva(long idReserva) throws NotFoundException {
		getReservaById(idReserva).cancelarReserva();
	}

	public List<Reserva> getReservasActivas() {
		return reservas.stream().filter(r -> r.estaActiva()).collect(Collectors.toList());
	}

	private Reserva getReservaById(long idReserva) throws NotFoundException {
		try {
			return this.getReservasActivas().stream().filter(r -> r.getIdReserva() == idReserva).findFirst().get();
		} catch (NoSuchElementException e) {
			throw new NotFoundException(ErrorConstants.RESERVA_NO_ENCONTRADA);
		}
	}

	
	// Getters & Setters
	public String getUsername() {
		return username;
	}
	
	public Long getIdUsuario() {
		return idUsuario;
	}
	
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setReservas(Set<Reserva> reservas) {
		this.reservas = reservas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Set<Reserva> getReservas() {
		return reservas;
	}
}
