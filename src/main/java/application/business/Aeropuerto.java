package application.business;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;


@Entity
@org.mongodb.morphia.annotations.Entity
@NodeEntity
public class Aeropuerto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@GraphId
	private Long idAeropuerto;
	
	private String ciudad;

	public Aeropuerto(Long idAeropuerto, String ciudad) {
		this.idAeropuerto = idAeropuerto;
		this.ciudad = ciudad;
	}
	
	protected Aeropuerto(){}

	
	//Getters & Setters
	public Long getIdAeropuerto() {
		return idAeropuerto;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	
	public String toString() {
		return ciudad;
	}
}