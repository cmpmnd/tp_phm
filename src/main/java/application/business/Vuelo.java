package application.business;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import application.utils.DateUtils;

@Entity
@org.mongodb.morphia.annotations.Entity
@NodeEntity
public class Vuelo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@org.mongodb.morphia.annotations.Id
	@GraphId
	private Long idVuelo;
	
	@Transient
	final private String name="Vuelo";//Para visualizacion Neo4J
	
	@Column(length = 30)
	private String aerolinea;
	
	@ManyToOne
	@RelatedTo(type="DESDE")
	@Fetch
	private Aeropuerto origen;
	
	@ManyToOne
	@RelatedTo(type="HASTA")
	@Fetch
	private Aeropuerto destino;
	
	@GraphProperty(propertyType = Long.class)
	private LocalDateTime fechaDespegue;
	
	@GraphProperty(propertyType = Long.class)
	private LocalDateTime fechaAterrizaje;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@RelatedTo(type="TRAMO")
	private Set<Tramo> tramos = new HashSet<>();
	
	@org.mongodb.morphia.annotations.Transient
	@OneToMany(fetch=FetchType.LAZY, mappedBy="vueloPertenece", cascade=CascadeType.ALL)
	@RelatedTo(type="ASIENTO")
	private Set<Asiento> asientos = new HashSet<>();
	
	protected Vuelo(){}
	
	private Vuelo(VueloBuilder builder) {
		this.idVuelo = builder.idVuelo;
		this.aerolinea = builder.aerolinea;
		this.origen = builder.origen;
		this.destino = builder.destino;
		this.fechaDespegue = builder.fechaDespegue;
		this.fechaAterrizaje = builder.fechaAterrizaje;
		this.tramos = builder.tramos.stream().collect(Collectors.toSet());
	}
	
	public int getCantidadTramos() {
		return tramos.size();
	}
	
	public void setTramos(List<Tramo> tramos) {
		this.tramos = new HashSet<>();
		this.tramos.addAll(tramos);
	}

	public List<Asiento> getAsientosLibres() {
		return asientos.stream()
			.filter(asiento -> !asiento.isEstaReservado())
			.collect(Collectors.toList());
	}
	
	public String getStringFechaDespegue() {
		return DateUtils.dateTimeToString(fechaDespegue);
	}
	
	public String getStringFechaAterrizaje() {
		return DateUtils.dateTimeToString(fechaAterrizaje);
	}
	
	//Setters & Getters
	public Long getIdVuelo() {
		return idVuelo;
	}
	public String getAerolinea() {
		return aerolinea;
	}
	public Aeropuerto getOrigen() {
		return origen;
	}
	public LocalDateTime getFechaDespegue() {
		return fechaDespegue;
	}
	public LocalDateTime getFechaAterrizaje() {
		return fechaAterrizaje;
	}
	public Aeropuerto getDestino() {
		return destino;
	}
	public List<Tramo> getTramos() {
		return tramos.stream().collect(Collectors.toList());
	}
	
	public Asiento getAsiento(String clave) {
		return asientos.stream()
				.filter(asiento -> asiento.toString().equals(clave))
				.findFirst()
				.get();
	}
	
	public Set<Asiento> getAsientos() {
		return asientos;
	}
	
	public String getName() {
		return name;
	}


	public static class VueloBuilder {
		private Long idVuelo;
		private String aerolinea;
		private Aeropuerto origen;
		private Aeropuerto destino;
		private LocalDateTime fechaDespegue;
		private LocalDateTime fechaAterrizaje;
		private List<Tramo> tramos = new ArrayList<Tramo>();

		public VueloBuilder idVuelo(Long idVuelo) {
			this.idVuelo = idVuelo;
			return this;
		}
		
		public VueloBuilder aerolinea(String aerolinea) {
			this.aerolinea = aerolinea;
			return this;
		}

		public VueloBuilder origen (Aeropuerto origen) {
			this.origen = origen;
			return this;
		}
		
		public VueloBuilder destino (Aeropuerto destino) {
			this.destino = destino;
			return this;
		}
		
		public VueloBuilder fechaDespegue (LocalDateTime fechaDespegue) {
			this.fechaDespegue = fechaDespegue;
			return this;
		}
		
		public VueloBuilder fechaAterrizaje (LocalDateTime fechaAterrizaje) {
			this.fechaAterrizaje = fechaAterrizaje;
			return this;
		}
		
		public VueloBuilder agregarTramo (Tramo tramo) {
			tramos.add(tramo);
			return this;
		}

		public Vuelo build() {
			return new Vuelo(this);
		}
	}
}
