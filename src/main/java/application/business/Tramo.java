package application.business;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import application.utils.DateUtils;

@Entity
@org.mongodb.morphia.annotations.Entity
@NodeEntity
public class Tramo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@org.mongodb.morphia.annotations.Id
	@GraphId
	private Long idTramo;
	
	@GraphProperty(propertyType = Long.class)
	private LocalDateTime fechaLlegada;
	
	@GraphProperty(propertyType = Long.class)
	private LocalDateTime fechaSalida;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@RelatedTo(type="DESTINO_INTERMEDIO")
	@Fetch
	private Aeropuerto destinoIntermedio;
	
	protected Tramo(){}
	
	public Tramo(Long idTramo, LocalDateTime horaLlegada, LocalDateTime horaSalida, Aeropuerto destinoIntermedio) {
		this.idTramo= idTramo;
		this.fechaLlegada = horaLlegada;
		this.fechaSalida = horaSalida;
		this.destinoIntermedio = destinoIntermedio;
	}
	
	public String getStringFechaLlegada(){
		return DateUtils.dateTimeToString(fechaLlegada);
	}
	
	public LocalDateTime getFechaLlegada() {
		return fechaLlegada;
	}
	public void setFechaLlegada(LocalDateTime horaLlegada) {
		this.fechaLlegada = horaLlegada;
	}
	public LocalDateTime getFechaSalida() {
		return fechaSalida;
	}
	public void setFechaSalida(LocalDateTime horaSalida) {
		this.fechaSalida = horaSalida;
	}
	public Aeropuerto getDestinoIntermedio() {
		return destinoIntermedio;
	}
	public void setDestinoIntermedio(Aeropuerto destinoIntermedio) {
		this.destinoIntermedio = destinoIntermedio;
	}
}
