package application.business;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import application.utils.DateUtils;

@Entity
@RelationshipEntity(type="RESERVO")
public class Reserva implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@GraphId
	private Long idReserva;

	@GraphProperty(propertyType = Long.class)
	private LocalDateTime fechaReserva;

	@GraphProperty(propertyType = Long.class)
	private LocalDateTime fechaCancelacion;

	@ManyToOne
	@JoinColumn(name="idUsuario")
	@StartNode 
	private Usuario usuarioReserva;

	@ManyToOne
	@EndNode
	@Fetch
	private Asiento asientoReservado;

	public Reserva(Usuario usuarioReserva, Asiento asientoReservado) {
		this.fechaReserva = LocalDateTime.now();
		this.usuarioReserva = usuarioReserva;
		this.asientoReservado = asientoReservado;
		asientoReservado.reservar();
	}
	
	protected Reserva() {}

	public boolean estaActiva() {
		return (fechaCancelacion == null);
	}

	public void cancelarReserva() {
		asientoReservado.cancelarReserva();
		fechaCancelacion = LocalDateTime.now();
	}

	public String getCiudadOrigen() {
		return asientoReservado.getCiudadOrigen();
	}

	public String getCiudadDestino() {
		return asientoReservado.getCiudadDestino();
	}

	public String getFechaSalida() {
		return DateUtils.dateTimeToString(asientoReservado.getFechaSalida());
	}

	public String getFechaLlegada() {
		return DateUtils.dateTimeToString(asientoReservado.getFechaLlegada());
	}

	public int getCantidadTramos() {
		return asientoReservado.getCantidadTramos();
	}

	// Setters & Getters
	
	public Long getIdReserva() {
		return idReserva;
	}
	
	public void setIdReserva(Long idReserva) {
		this.idReserva = idReserva;
	}

	public LocalDateTime getFechaReserva() {
		return fechaReserva;
	}

	public void setFechaReserva(LocalDateTime fechaReserva) {
		this.fechaReserva = fechaReserva;
	}

	public LocalDateTime getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(LocalDateTime fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public Usuario getUsuarioReserva() {
		return usuarioReserva;
	}

	public void setUsuarioReserva(Usuario usuarioReserva) {
		this.usuarioReserva = usuarioReserva;
	}

	public Asiento getAsientoReservado() {
		return asientoReservado;
	}

	public void setAsientoReservado(Asiento asientoReservado) {
		this.asientoReservado = asientoReservado;
	}

	public Vuelo getVuelo() {
		return this.asientoReservado.getVueloPertenece();
	}
}
