package application.constants;

public final class Profil {
    public static final String INMEMORY="inMemory";
    public static final String HIBERNATE="hibernate";
    public static final String NEO4J="neo4J";
    
    
    private Profil() {
    	//Para que no se pueda instanciar
    }
}
