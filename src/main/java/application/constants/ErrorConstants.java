package application.constants;

public class ErrorConstants {
	public static final String ASIENTO_YA_RESERVADO="El asiento se encuentra reservado";
	public static final String RESERVA_NO_ENCONTRADA="No se ha encontrado la reserva solicitada";
    
    private ErrorConstants() {}
}
