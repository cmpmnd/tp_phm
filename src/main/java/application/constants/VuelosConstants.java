package application.constants;

public final class VuelosConstants {
	public static final String DATE_FORMAT="dd-MM-yyyy";
	public static final String TIME_FORMAT="HH:mm";
    public static final String DATE_TIME_FORMAT="dd-MM-yyyy HH:mm";
    
    public static final String NUMBERS_PATTERN="^[0-9.]*$";
    
    private VuelosConstants() {
    	//Para que no se pueda instanciar
    }
}
