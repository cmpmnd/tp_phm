package application.constants;

public final class CiudadConstants {
	public static final String BUENOS_AIRES="Buenos Aires";
	public static final String TUCUMAN="Tucumán";
	public static final String SAO_PABLO="Sao Pablo";
	public static final String CHILE="Chile";
	public static final String CANADA="Canadá";
	public static final String MEXICO="Mexico";
    
    private CiudadConstants() {
    	//Para que no se pueda instanciar
    }
}
