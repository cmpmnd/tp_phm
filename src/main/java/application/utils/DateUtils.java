package application.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import application.constants.VuelosConstants;

public final class DateUtils {
	public static final String dateTimeToString(LocalDateTime localDateTime) {
		return localDateTime.format(DateTimeFormatter.ofPattern(VuelosConstants.DATE_TIME_FORMAT));
	}
	
	public static final String dateToString(LocalDate localDate) {
		return localDate.format(DateTimeFormatter.ofPattern(VuelosConstants.DATE_FORMAT));
	}
	
	public static final LocalDateTime localDateToLocalDateTimeEarly(LocalDate localDate) {
		if (localDate==null)
			return null;
		return LocalDateTime.of(localDate.getYear(), localDate.getMonth(), localDate.getDayOfMonth(), 0, 0);
	}
	
	public static final LocalDateTime localDateToLocalDateTimeLate(LocalDate localDate) {
		if (localDate==null)
			return null;
		return LocalDateTime.of(localDate.getYear(), localDate.getMonth(), localDate.getDayOfMonth(), 23, 59);
	}
	
	public static final LocalDate dateToLocalDate(Date date) {
		if (date==null)
			return null;
		
		Instant instant = Instant.ofEpochMilli(date.getTime());
		return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
	}
	
	public static final Date localDateToDate(LocalDate localDate) {
		if (localDate==null)
			return null;
		
		Instant instant = localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(instant);
	}
	
	//https://blog.tompawlak.org/java-8-conversion-new-date-time-api
}