package application.utils;

public final class NeoSearchBuilder {
	/**Agrega un filtro a una query Neo4J
	 * @param actualQuery la query actual que se está construyendo
	 * @param newFilter el nuevo filtro que se quiere agregar a la query
	 * @return la query (String) con el filtro agregado; 
	*/
	public final static String addFilterToQuery(String actualQuery, String newFilter) {
		actualQuery+= getConnectorForParams(actualQuery);
		actualQuery+= newFilter;
		return actualQuery;
	}
	
	/**Agrega una variable de retorno a una query Neo4J
	 * @param actualQuery la query actual que se está construyendo
	 * @param newVariable el nombre de la variable que se quiere agregar a la query
	 * @return la query (String) con la variable de retorno agregada;
	*/
	public final static String addReturnVariable(String actualQuery, String newVariable) {
		actualQuery+= getConnectorForReturns(actualQuery);
		actualQuery+= newVariable;
		return actualQuery;
	}
	
	private static boolean contains(String source, String text) {
		return source.toUpperCase().contains(text);
	}
	
	private static String getConnectorForParams(String actualQuery) {
		return contains(actualQuery, "WHERE")? " AND " : " WHERE ";
	}
	
	private static String getConnectorForReturns(String actualQuery) {
		return contains(actualQuery, "RETURN")? ", " : " RETURN ";
	}
	
	private NeoSearchBuilder() {
		//Prevent initialization;
	}
}
