## **Vuelos APP** es una aplicación que permite gestionar reservas de vuelos. ##
**Está desarrollada en Java 1.8, con el framework SpringMVC 4.**

---

Ésta aplicación está compuesta por las siguientes pantallas:

**Login**: autenticación con usuario y contraseña;

**Pantalla principal**: con las reservas efectuadas por el usuario autenticado;

**Pantalla de búsqueda de vuelos**: permite aplicar diferentes filtros como origen, destino, fechas, etc.;

**Pantalla de reserva de asientos**: muestra que asientos están libres, el monto de cada uno y llevar a cabo la reserva.

---
La aplicación tiene diferentes formas de persistencia y por cada una de ellas se corresponde un perfil:

* ***inMemory***: los datos se procesan y almacenan en *memoria*. (Objetos);
* ***hibernate***: los datos se procesan y almacenan en una *base de datos relacional*. (SQL);
* ***neo4J***: los datos se procesan y almacenan en una *base de datos orientada a grafos*. (NoSQL).

Ademas, la aplicación permite registrar los logs de las búsquedas de vuelos y asientos, lo cual se persiste en una *base orientada a documentos*. (NoSQL)

### **FUNCIONAMIENTO** ###
Tanto la usabilidad de la aplicación (logeo, búsqueda de vuelos, reservas de asientos), 
como los tests y la inicialización de datos dependen del perfil configurado al iniciar la aplicación,
y son tratados de manera ***polimórfica***.

1. Indiferentemente al perfil elegido, al ejecutar la aplicación se genera un juego de datos (algunos aleatorios) en memoria.
2. Se lleva a cabo la estrategia ***create if not exists***, es decir: según el perfil elegido, **si no existe ni un vuelo en esa base**, se entiende que se requiere inicialización: **todo el juego de datos generado en memoria se va a persistir.** Si existe almenos un vuelo en la base, la inicializacion es salteada.

Al ejecutar los *tests*, se mantiene la misma lógica: se genera el juego de datos en memoria y se los persiste de ser necesario.

Éste proceso no se repite por cada test. Para que no tengan efecto posterior, los tests que modifican datos son Transaccionales: se persiste, se valida el resultado y se hace *rollback*.

---

# **INSTALACIÓN** #
**1) Instalar IDE**

Se recomienda instalar el **Eclipse IDE for Java EE Developers**:
[Eclipse downloads](https://www.eclipse.org/downloads/)

**2) Instalar java 8**

Se recomienda instalar la versión: jdk-8u101-*:
[Java 8 downloads](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

**3) Configurar IDE**

Asumiento que ya se definió el *Workspace*, configurar el IDE de la siguiente manera:

**3.1) (OPCIONAL) Vincular la JDK, NO JRE**

```
#!html

Windows-->Preferences-->Java-->Installed JREs-->Add...;
```
**3.2) Configurar el encoding**

Se propone UTF-8, ya que es un encoding internacional. Por defecto, Eclipse usa el encoding del sistema operativo.

```
#!html

Windows-->Preferences-->General-->Workspace, abajo a la izquierda en "Text file encoding", Other: UTF-8;
Windows-->Preferences-->General-->Content Types-->Text-->Java Properties File, abajo en "Default encoding: ", UTF-8
```


**3.3) (OPCIONAL) Instalar "Thymeleaf Eclipse Plugin"**

```
#!html
Help-->Install New Software..-->Add...
Nombre: ThymeleafPlugin
Dirección: http://www.thymeleaf.org/eclipse-plugin-update-site/
```

**4) Importar el proyecto**

Importar el proyecto con GIT (que ya viene instalado en el Eclipse Mars/Neon), importarlo como General Project (no Wizard) y convertirlo en *Maven Project*.

El proceso puede tardar, ya que se descargan las dependencias.

---

# **CONFIGURACIÓN** #
La configuración del proyecto se administra desde el archivo 
```
#!html
application.properties
```

**Por defecto** se persiste en memoria, y **no es necesario configurar ninguna base**, aunque si se elije otro perfil, va a ser necesario configuraciones adicionales.

### PERFILES: ###

---

```
#!html
spring.profiles.active=inMemory
```
Perfil por defecto, no requiere configuración adicional.

---

```
#!html
spring.profiles.active=hibernate
```
Para el perfil ***hibernate***, se deberá levantar una base de datos relacional, se recomienda *MySQL*:

Instalar MySQL Server e indicar en la configuración **URL**, **USERNAME** y **PASSWORD**;

(En la URL se indica el tipo de base, la direccion, el puerto y el schema)

---

```
#!html
spring.profiles.active=neo4J
```
Para el perfil ***neo4J***, se utiliza una base de grafos, y se levanta *embedded* (no server) por lo que **solo se necesita definir un directorio** en el cual se van a guardar los archivos.

De no existir el directorio indicado, la aplicación se encargará de crearlo.

**IMPORTANTE:** Para poder visualizar esta base, se deberá instalar la versión: **Neo4j Community Edition 2.2.9**

---

### USO DE LOGs: ###
```
#!html
spring.configuration.use_search_logs=true
```
Si se indica ***use_search_logs=true***, se deberá configurar un servidor MongoDB (mongod.exe) e indicar en la configuración **HOST** y **PORT**.

---

# **EJECUCIÓN** #
Para levantar el proyecto, ir al archivo **Application.java**, 
```
#!html
click derecho--> Run As --> Java Application
```
La aplicación se levantará con un embedded Tomcat.

Para ingresar, ir a la URL: 
[http://localhost:8080/login](http://localhost:8080/login)

**Usuario y Contraseña** por defecto: dodain

Se recomienda ingresar con **Google Chrome**, ya que se están usando algunos tags de *html5* que no están soportados aún por todos los navegadores.

Ej: [type date](http://caniuse.com/#search=date)

---

*Trabajo práctico realizado por Salomon Leandro, Petelski Juan y Balboa Daniel.*